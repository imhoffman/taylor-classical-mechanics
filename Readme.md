Here are a few simple solutions to a handful of numerical problems from [Taylor's standard text on Classical Mechanics](https://www.uscibooks.com/taylor2.htm).
Having taught this course several times, I am wary of making solutions available on the internet.
So, I am not posting many of my course solutions here&mdash;merely a few worked examples so that students have a bit of code from which to work.

In Taylor, students are encouraged to invoke the solver functionality that is built into Mathematica or Matlab; my more modern students use Maxima or SciPy.
Too often, however, black-boxing the methods leaves the student unable to obtain or to appreciate a solution.
The point of this repository is to teach some methods alongside the physics.
The numerical methods used here are coded from scratch&mdash;usually a [fixed-step, fourth-order Runge&ndash;Kutta method](https://aip.scitation.org/doi/pdf/10.1063/1.4823060).
In order to make the lessons as rich and as portable as possible, a variety of languages are used for the coding (so far, Fortran, Go, Clojure, and Python; soon C and Java).
Furthermore, language-specific plotting APIs/bindings are *not* used; rather, each code outputs a simple text file that can be subsequently plotted with [gnuplot](http://www.gnuplot.info/) (maybe even [online](http://gnuplot.respawned.com/))&mdash;for some problems, several different, independent codes are provided, the output of each of which can be plotted with the same provided gnuplot script.

