!!
!! parameters and types
!!
  module types
   use defs                             ! defines bit widths
   implicit none
   integer, parameter :: Nsteps = 2048  ! number of steps
   real(kind=rw), parameter :: g  = 9.82_rw
   real(kind=rw), parameter :: pi = 3.14159265358979323846264338327950_rw
   real(kind=rw), parameter :: R  = 5.00_rw
  end module types
!!
!! subprograms
!!
  module subs
   use defs
   use types
   implicit none

   contains

   ! the problem at hand; the vector f is phi, omega and df is omega, alpha 
   pure function Y( f, odebool ) result ( vec_out )
    real(kind=rw), intent(in)  :: f(:)
    logical, intent(in)        :: odebool
    real(kind=rw), allocatable :: df(:), vec_out(:)
    integer                    :: ndim

    ndim = size(f)
    allocate( df(ndim) )     ! will `deallocate` when out of scope at `return`
    if ( odebool ) then
      allocate( vec_out( ndim ) )
    else
      allocate( vec_out(ndim/2) )
    endif

    df(1) = f(2)
    df(2) = -g/R*sin( f(1) )

    if ( odebool ) then
      vec_out = df
    else
      vec_out = df(2)
    endif

    deallocate( df )
    return
   end function Y
  end module subs

!!
!! main
!!
  program main
    use defs
    use types
    use subs
    use rk4
    use verlet
    implicit none   
    integer, parameter :: p = 2          ! second deriv. times one dimension
    real(kind=rw) :: ti, tf, h, t, phi500, phi510
    real(kind=rw) :: soln50(p), soln51(p)
    real(kind=rw) :: phi50, phi51
    ! for Verlet
    real(kind=rw) :: a50_prev(p/2), a50_next(p/2)
    real(kind=rw) :: a51_prev(p/2), a51_next(p/2)
    character :: envv
    logical   :: use_rk4 = .false.
    integer   :: i 

    ! boundary conditions
    ti = 0.0_rw
    tf = 4.0_rw * 2.0_rw*pi/sqrt(g/R)       ! a few periods
    phi500    = +20.0_rw*pi/180.0_rw
    phi510    = +90.0_rw*pi/180.0_rw
    soln50(1) = phi500                    ! 1-50
    soln50(2) = 0.0_rw                    ! released from rest
    soln51(1) = phi510                    ! 1-51
    soln51(2) = 0.0_rw                    ! released from rest

    h = ( tf - ti ) /real( Nsteps, kind=rw)

    !!  `recl` defaults to 40
    open(10,recl=180,file="soln50.txt")
    open(11,recl=180,file="soln51.txt")
    !! use  `$ export SOLN_BY_VERLET=1` at the shell
    !! then `$ unset SOLN_BY_VERLET` to use RK4
    call getenv( "SOLN_BY_VERLET", envv )
    if ( envv .eq. " " ) then
      use_rk4 = .true.
      write(6,*) " Using Runge--Kutta solution method."
    else
      use_rk4 = .false.
      a50_next = Y( soln50, .false. )
      a51_next = Y( soln51, .false. )
      write(6,*) " Using Verlet solution method."
    endif
    t = ti
    do while ( t .le. tf )     ! do i = 1, Nsteps
      if ( use_rk4 ) then
       call rk4step(h, soln50, Y)
       call rk4step(h, soln51, Y)
     else
       a50_prev = a50_next
       a51_prev = a51_next
       call verlet_step( h, soln50, a50_prev, a50_next, Y )
       call verlet_step( h, soln51, a51_prev, a51_next, Y )
     endif
     phi50 = soln50(1)
     phi51 = soln51(1)
     t = t + h
     write(10,*)  t, phi50*R, phi500*R*cos( sqrt(g/R)*t )
     write(11,*)  t, phi51*R, phi510*R*cos( sqrt(g/R)*t )
    end do
    close(10)
    close(11)

    !stop
  end program main

