#
# plots for both 1-50 and 1-51
#
##  boundary limits
##   R must match code file
R = 5
Tmax = +18.0
Smin = -pi/2.0*R*1.1
Smax = +pi/2.0*R*1.2
##  first plot file
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set key inside top right
set output '1-50.png'
set xzeroaxis
set xlabel 'time (s)'
set ylabel "position (arc length) from bottom of pipe (m)"
set title sprintf('Problem 1-50 for radius {/:Italic R} = %d m', R)
plot [0.0:Tmax] [Smin:Smax] 'soln50.txt' using 1:2 with lines title 'numerical', 'soln50.txt' using 1:3 with lines title 'small-angle analytical'
reset
##  second plot file ... same limits for comparison
#set terminal png enhanced size 1024,768 crop font "Times Roman, 24" linewidth 2
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica,24" linewidth 2
set key inside top right
set output '1-51.png'
set xzeroaxis
set xlabel 'time (s)'
set ylabel 'position (arc length) from bottom of pipe (m)'
set title sprintf('Problem 1-51 for radius {/:Italic R} = %d m', R)
plot [0.0:Tmax] [Smin:Smax] 'soln51.txt' using 1:2 with lines title 'numerical', 'soln51.txt' using 1:3 with lines title 'small-angle analytical'
