Based on [the exposition in Example 1.2](pages.pdf), the following plots are obtained.
The limits of the utility of the small-angle approximation are apparent.

![1-50.png](1-50.png)

![1-51.png](1-51.png)
