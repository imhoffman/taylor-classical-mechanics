#!/bin/bash

prog="1-51"

if [ "$1" = "ifort" ]; then
  FC="ifort"
elif [ "$1" = "pgfortran" ]; then
  FC="pgfortran"
else
  FC="gfortran"
fi

rm -f sol*txt 1-5?.png
make clean
make FC=$FC

./$prog
gnuplot $prog.gnuplot
display $prog.png &
