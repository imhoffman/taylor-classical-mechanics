The [solution](latex/5-18.pdf) has [Problems 4-33 and 5-13 as a run-up to Problem 5-18](images/pages.pdf).

# Problem 5-13

Based on [an expanded reading of Problem 5-13](images/pages.pdf) as described in [the solution](latex/5-18.pdf), the following plots are obtained.
As with [Problem 1-51](https://gitlab.com/imhoffman/taylor-classical-mechanics/tree/master/problems/1-51), the limits of the utility of the small-displacement approximation are apparent if suitable boundary conditions are chosen.
For the constants coded here (for example, in [5-13.clj](code/5-13.clj)), the approximation is good and the numerical solution shows the expected number of oscillations in the plotted interval.

![5-13-potential.png](images/5-13-potential.png)

![5-13-time.png](images/5-13-time.png)

![5-13-orbit.png](images/5-13-orbit.png)

# Problem 5-18

When the springs are in compression at *x*=0, *y*=0, this problem gets interesting.

![5-18-potential.png](images/5-18-potential.png)

With friction, it gets even more interesting.

![5-18-multi-mu.png](images/5-18-multi-mu.png)

Above is plotted the trajectory for the first 5&nbsp;s given the conditions *m* = 165 g, *k* = 28 N/m, *a* = 90 cm, *L0* = 115 cm, *mu* = 0.22, *xinit* = 20 cm, *yinit* = -115 cm.

Below is 25&nbsp;s of the solution for initial conditions very near the top of the *y* well (*m* = 65 g, *k* = 28 N/m, *a* = 90 cm, *L0* = 115 cm, *mu* = 0.04, *xinit* = 20 cm, *yinit* = 109 cm), the oscillations are initially anharmonic and lower in frequency than "expected."
That is&mdash;as in [Problem 1-51](https://gitlab.com/imhoffman/taylor-classical-mechanics/tree/master/problems/1-51)&mdash;because the *y* well departs from parabolic (by being wider) near the top at (0,0), the *y* oscillations are initially relatively long and then get higher in frequency as the friction serves to localize the oscillations near the more-harmonic bottom of the well.
The spectrogram below is generated with the code in [`spectro.py`](code/5-18-spectro.py) using SciPy and MatplotLib.

![5-18-chirpmulti.png](images/5-18-chirpmulti.png)
![5-18-chirpspectro.png](images/5-18-chirpspectro.png)

