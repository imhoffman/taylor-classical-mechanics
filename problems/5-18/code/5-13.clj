(require '[clojure.string :as str])
(load-file "../../../utilities/taylor.clj")

;;  https://clojure.org/guides/learn/hashed_colls#_looking_up_by_key
;;  https://clojure.org/reference/reader#_literals
(def constants {
   :pi      3.1415926535897932384626433832795028841971
   :nsteps  8192
   :morb    0.250	;; mass of particle (kg)
   :l       0.750	;; dimensionless constant
   :R       0.25	;; scale length (m)
   :U0      12.0	;; scale energy (J)
   } )

;;  f is r, rdot, phi, phidot
(defn Y [f]
  (vector
    (f 1)
    (-
     (* (f 0) (f 3) (f 3))
     (* (/ (constants :U0) (constants :morb))
        (-
         (/ 1.0 (constants :R))
         (* (constants :l) (constants :l)
            (/ (constants :R) (f 0) (f 0))))))
    (f 3)
    0.0))


;;  main program
(let [ti 0.0
      tf 1.50
      h (/ (- tf ti) (constants :nsteps))]
  (spit "soln.txt"         ; write header line to data file
        (str/join " " (list
          ti tf
          (constants :morb) (constants :U0) (constants :l) (constants :R) 
          (/ (Math/sqrt (/ (* 2.0 (constants :U0)) (constants :morb) (constants :l)
                        (constants :R) (constants :R))) 2.0 (constants :pi))
          \newline))
        :append true)
  (loop [t ti
         soln (vector
                (+ (constants :R) 0.01)       ;; r_initial
                0.00                          ;; rdot_initial
                (/ (constants :pi) -1.10)     ;; phi_initial
                (/ (constants :pi) +0.80)) ]  ;; phidot_initial
    (let [r    (soln 0)
          phi  (soln 2)]
      (spit "soln.txt"
            (str/join " " (list
              t
              r
              phi
              (* r (Math/cos phi))
              (* r (Math/sin phi))
              \newline))
            :append true)
      (when (< t tf)
        (recur (+ t h) (rk4step h soln Y))))))

