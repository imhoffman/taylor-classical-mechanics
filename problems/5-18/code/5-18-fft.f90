program main

!*****************************************************************************80
!
!! MAIN is the main program for FFTPACK5.1D_PRB.
!
!  Discussion:
!
!    FFTPACK5.1D_PRB tests the FFTPACK5.1D library.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    15 November 2011
!
!  Author:
!
!    John Burkardt
!
  implicit none

!*****************************************************************************80
!
!! TEST09 tests RFFT1B, RFFT1F and RFFT1I.
!
!  Licensing:
!
!    This code is distributed under the GNU LGPL license.
!
!  Modified:
!
!    15 November 2011
!
!  Author:
!
!    John Burkardt
!
  integer ( kind = 4 ), parameter :: n = 8192
  integer ( kind = 4 ) :: ier, inc, lenr, lensav, lenwrk, i
  real ( kind = 8 )    :: t(n), x(n), y(n), f(n)
  real ( kind = 8 )    :: Fs
  real ( kind = 8 ), allocatable, dimension ( : ) :: work
  real ( kind = 8 ), allocatable, dimension ( : ) :: wsave
!
!  Set and initialize work vectors.
!
  lensav = n + int ( log ( real ( n, kind = 8 ) ) / log ( 2.0D+00 ) ) + 4
  lenwrk = n
  allocate ( work(1:lenwrk) )
  allocate ( wsave(1:lensav) )
  call rfft1i ( n, wsave, lensav, ier )

  ! read data in to t, x, y
  open( 10, file="soln.txt" )
  read(10,*) Fs         ! dump header
  do i = 1, n
   read(10,*) t(i), x(i), y(i)
  end do
  close(10)
  Fs = 1.0D0/( t(2) - t(1) )       ! compute sampling freq
!
!  Compute the FFT coefficients.
!
  inc = 1
  lenr = n
  call rfft1i ( n, wsave, lensav, ier )
  call rfft1f ( n, inc, x, lenr, wsave, lensav, work, lenwrk, ier )
  inc = 1
  lenr = n
  call rfft1i ( n, wsave, lensav, ier )
  call rfft1f ( n, inc, y, lenr, wsave, lensav, work, lenwrk, ier )

  do i = 1, n
   f(i) = Fs/2.0D0 * ( i - 1.0D0 )
  end do

  !  write out fft data
  open( 11, file="fft.txt" )
  write( 11, *) n       ! header for gnuplot
  do i = 1, n
   write( 11, *) f(i), x(i), y(i)
  end do
  close(11)

  deallocate ( work )
  deallocate ( wsave )

  return
end
