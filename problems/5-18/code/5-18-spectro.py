import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

#  file i/o
m, k, a, L0, ti, tf, muk, y0, f0x, f0y = np.genfromtxt("soln.txt", max_rows=1)   # header
txy = np.loadtxt("soln.txt", skiprows=1)                           # series data

f_o = np.sqrt(k/m)/2.0/3.14159      # rough estimate of characteristic frequency
fs  = 1.0/( txy[1,0] - txy[0,0] )   # compute sampling rate from data

#  physical sanity checks
if ( txy[-1,0] - txy[0,0] ) < 2.0/f_o : print( "\n Warning: time series is too short in duration; change tf\n\n" )
if fs < 2.0*f_o : print( "\n Warning: sampling rate is too slow; change Nsteps\n\n" )

win_time   = 5.0/f_o                    # window (in sec) as multiple of tau = 1/f_o
win_length = int( win_time     * fs )   # window time as a number of samples
overlap    = int( win_length/1.5 )      # overlap as a fraction of the window
nfft       = 2**( ( txy.shape[0] - 1 ).bit_length() + 2 )  # oversample for smoothness: four times the length of the data
#nfft       = 2**( ( win_length - 1 ).bit_length() + 2 )  # oversample for smoothness: four times the length of the window

#  separate x and y analyses
fx, tx, Sx = signal.spectrogram(txy[:,1], fs=fs, window=signal.get_window('hann',win_length), nperseg=win_length, noverlap=overlap, nfft=nfft, scaling='spectrum' )
fy, ty, Sy = signal.spectrogram(txy[:,2], fs=fs, window=signal.get_window('hann',win_length), nperseg=win_length, noverlap=overlap, nfft=nfft, scaling='spectrum' )

#  subplots for x and y
fig, (Xplt,Yplt) = plt.subplots(1, 2, sharey=False, tight_layout=True)
#  y
Yplt.pcolormesh(ty, fy, Sy)
Yplt.set_xlabel('time (s)')
Yplt.set_ylabel('frequency of $y$ motion (Hz)')
Yplt.set_title('5-18: $y$ motion')
#  x
Xplt.pcolormesh(tx, fx, Sx)
Xplt.set_xlabel('time (s)')
Xplt.set_ylabel('frequency of $x$ motion (Hz)')
Xplt.set_title('5-18: $x$ motion')
#  sharey doesn't affect colormesh
Yplt.set_ylim( [ 0.0, 1.75*f_o ] )    # plot the frequencies of physical relevance
Xplt.set_ylim( [ 0.0, 1.75*f_o ] )

plt.show()

