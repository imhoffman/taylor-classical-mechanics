#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"taylor.h"

const    int Nsteps = 16384;
const    int   Neqs =     4;     // two coords, one derivative each
const double   mass =   0.165;   // mass (kg)
const double     k1 =  28.00;    // spring constant (N/m)
const double     k2 =  28.00;    // springs same as each other
const double      a =   0.90;    // initial spring length---wall to origin (m)
const double     L0 =   1.15;    // relaxed spring length (m)
const double    muk =   0.220 ;  // kinetic friction coefficient
const double   grav =   9.82;    // little g (m/s/s)


//  the diff eq system for RK4/Verlet
void
Y ( double *f,     //  f is x, v_x, y, v_y
    double *acc,   //  separate accel return for Verlet
    const int length_f )
{

  double* df = (double *) malloc( (size_t)length_f * sizeof(double) );
  double beta1, beta2;

  beta1 = sqrt( ( a + f[0] )*( a + f[0] ) + f[2] * f[2] );
  beta2 = sqrt( ( a - f[0] )*( a - f[0] ) + f[2] * f[2] );

  df[0] = f[1];
  df[1] = (    k2*( beta2 - L0 )*( a - f[0] )/beta2 
             - k1*( beta1 - L0 )*( a + f[0] )/beta1
             - muk*mass*grav* f[1]
                / sqrt( f[1] * f[1] + f[3] * f[3] ) ) / mass;
  df[2] = f[3];
  df[3] = (    k1*( beta1 - L0 )* f[2] / beta1
             + k2*( beta2 - L0 )* f[2] / beta2
             + muk*mass*grav* f[3]
                / sqrt( f[1] * f[1] + f[3] * f[3] ) ) / -mass;

  //  populate acc for Verlet
  for ( int i=0; i<length_f/2; i++ )
    acc[i] = df[2*i+1];

  //  overwrite f for RK4
  memcpy( f, df, (size_t)length_f*sizeof(double) );

  free( df );
  return;
}


//
//  main program
//
int main ( void ) {

  FILE* filename = fopen( "soln.txt", "w" );
  double ti, tf, h, x, y, t, vx, vy;
  double y0, f0x, f0y, Emech, Etot, Pfric, Efric=0.0;
  unsigned int use_rk4 = 0;
  double *x_vec     =(double *) malloc( sizeof(double) * Neqs/2 );
  double *v_vec     =(double *) malloc( sizeof(double) * Neqs/2 );
  double *xv_vec    =(double *) malloc( sizeof(double) * Neqs   );
  double *a_vec_old =(double *) malloc( sizeof(double) * Neqs/2 );
  double *a_vec_new =(double *) malloc( sizeof(double) * Neqs/2 );

  //  boundary conditions
  ti       =   0.0 ;
  tf       =   5.0 ;
  x_vec[0] =  +0.20     ;        //  x_init
  v_vec[0] =  +0.000000000000 ;  // vx_init
  x_vec[1] =  -1.150    ;        //  y_init
  v_vec[1] =  +0.000000000001 ;  // vy_init
     //  NB: zero total speed blows up the friction term in Y ...


  //  some values for plotting header ... not part of solution
  if ( L0 <= a ) {
   y0  = 0.0;
   f0x = sqrt( 2.0 * k1 / mass ) / 2.0 / 3.14159 ;
   f0y = sqrt( 2.0 * k1 * (1.0 - L0/a ) / mass ) / 2.0 / 3.14159 ;
  } else {       // guess at some representative frequencies
   y0  = sqrt( L0*L0 - a*a ) ;
   f0x = sqrt( 2.0 * k1 *  a/L0 / mass ) / 2.0 / 3.14159 ;
   f0y = sqrt( 2.0 * k1 * y0/L0 / mass ) / 2.0 / 3.14159 ;
  }
  fprintf( filename,
           "%.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f %.15f\n",
             mass,   k1,    a,   L0,   ti,   tf,  muk,   y0,  f0x,  f0y );
  //  end plotting header


  //  check env var for desired method, defaults to RK4
  if ( getenv( "SOLN_BY_VERLET" ) == NULL ) {
    printf( " Using Runge--Kutta solution method.\n" );
    use_rk4 = 1;
  } else {
    printf( " Using Verlet solution method.\n" );
    //  Verlet initialization
    for ( int i=0; i<Neqs/2; i++ ) {
      xv_vec[2*i+0] = x_vec[i];
      xv_vec[2*i+1] = v_vec[i];
    }
    Y( xv_vec, a_vec_new, Neqs );
  }

  //
  //  solution loop
  //
  t = ti;
  h = ( tf - ti ) / (double)Nsteps ;
  while ( t < tf ) {      // for ( int i=0; i<Nsteps; i++ ) {
    //  update diff eq and time step
    if ( use_rk4 ) {
      rk4step( h, x_vec, v_vec, Neqs/2, &Y ); 
    } else {
      memcpy( a_vec_old, a_vec_new, (size_t)(Neqs/2)*sizeof(double) );
      verlet_step( h, x_vec, v_vec, a_vec_old, a_vec_new, Neqs/2, &Y );
    }
    t  = t + h;

    //  unpack for external file outputs
    x  = x_vec[0];  y  = x_vec[1];
    vx = v_vec[0];  vy = v_vec[1];

    //  compute energy diagnostics for output
    Emech = 0.5*mass*( vx*vx + vy*vy )
        + 0.5*k1*( ( sqrt( (x-a)*(x-a) + y*y ) - L0 )
	          *( sqrt( (x-a)*(x-a) + y*y ) - L0 )
                 + ( sqrt( (x+a)*(x+a) + y*y ) - L0 )
		  *( sqrt( (x+a)*(x+a) + y*y ) - L0 ) );
    Pfric = muk * mass * grav * sqrt( vx*vx + vy*vy );
    Efric = Efric + Pfric*h;
    Etot  = Emech + Efric;

    //  write line to file
    fprintf( filename, "%.15f %.15f %.15f %.15f %.15f %.15f %.15f\n",
                           t,    x,    y, Etot, Emech, Efric, Pfric );
  }
  //  end solution loop


  //  close file
  fclose(filename);

  //  free malloc's
  free( x_vec );      free( v_vec );      free( xv_vec );
  free( a_vec_old );  free( a_vec_new );

  return 0;
}

