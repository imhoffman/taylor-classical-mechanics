#!/usr/bin/env python3
#  `utilities` must be symlinked in order to avoid relative importing

import numpy as np
import math
from utilities.rk4 import rk4step as odestep

class constants:
    Nsteps = 8192
    p      = 4           # two coords, one derivative each
    mass   = 0.065       # mass (kg)
    k1     =  8.00       # spring constant (N/m)
    k2     = k1          # springs same as each other
    a      = 0.90        # initial spring length---wall to origin (m)
    L0     = 1.15        # relaxed spring length (m)
    muk    = 0.22        # kinetic friction coefficient
    grav   = 9.82        # little g

#
#  the system of diff eqs for the problem at hand
def Y ( f ):
    df = np.zeros( len(f) )

    b1 = math.sqrt( ( constants.a + f[0] )*( constants.a + f[0] )
                   + f[2]*f[2] )
    b2 = math.sqrt( ( constants.a - f[0] )*( constants.a - f[0] )
                   + f[2]*f[2] )

    #  f is x, v_x, y, v_y
    df[0] = f[1]
    df[1] = (  constants.k2*( b2 - constants.L0 )*
                ( constants.a - f[0] )/b2 
             - constants.k1*( b1 - constants.L0 )*
                ( constants.a + f[0] )/b1
             - constants.muk*constants.mass*constants.grav*
                f[1]/math.sqrt( f[1]*f[1] + f[3]*f[3] ) ) / constants.mass
    df[2] = f[3]
    df[3] = (  constants.k1*( b1 - constants.L0 )* f[2] / b1
             + constants.k2*( b2 - constants.L0 )* f[2] / b2
             + constants.muk*constants.mass*constants.grav*
                f[3]/math.sqrt( f[1]*f[1] + f[3]*f[3] ) ) / -constants.mass

    return df

#
#  main program
#

# open file
filename = open( "soln.txt", "w" )

#  initialize state vector
soln = np.zeros( constants.p )

#  boundary conditions
ti      =   0.0
tf      =   5.0
soln[0] =  +0.18                   #  x_init
soln[1] =  +0.000001               # vx_init
soln[2] =  -1.19                   #  y_init
soln[3] =  +0.000001               # vy_init
           #  NB: zero speed in the denominator in Y will blow it up

h = ( tf - ti ) / constants.Nsteps

if constants.L0 <= constants.a :
  y0  = 0.0
  f0x = sqrt( 2.0 * constants.k1 / constants.mass ) / 2.0 / 3.14159
  f0y = sqrt( 2.0 * constants.k1
             * (1.0 - constants.L0/constants.a )
             / constants.mass ) / 2.0 / 3.14159
else:
  #  some guesses at frequencies ... still sorting out
  y0  = math.sqrt( constants.L0*constants.L0 - constants.a*constants.a ) 
  f0x = math.sqrt( 2.0 * constants.k1
             * constants.a/constants.L0 / constants.mass ) / 2.0 / 3.14159 
  f0y = math.sqrt( 2.0 * constants.k1
             *          y0/constants.L0 / constants.mass ) / 2.0 / 3.14159 

#  write header for gnuplot
filename.write( f"{constants.mass:.15f} {constants.k1:.15f} {constants.a:.15f} {constants.L0:.15f} {ti:.15f} {tf:.15f} {constants.muk:.15f} {y0:.15f} {f0x:.15f} {f0y:.15f}\n" )

Efric = 0.0
#  solution loop
for i in range( constants.Nsteps ):
  #  update vector
  soln = odestep( h, soln, Y )
  x  = soln[0]
  vx = soln[1]
  y  = soln[2]
  vy = soln[3]
  t  = ti + h*( i + 1.0 )
  #  determine energy diagnostics for output
  Emech =( 0.5*constants.mass*( vx*vx + vy*vy )
          + 0.5*constants.k1*(
             ( math.sqrt( (x-constants.a)*(x-constants.a) + y*y ) - constants.L0 )
            *( math.sqrt( (x-constants.a)*(x-constants.a) + y*y ) - constants.L0 )
           + ( math.sqrt( (x+constants.a)*(x+constants.a) + y*y ) - constants.L0 )
            *( math.sqrt( (x+constants.a)*(x+constants.a) + y*y ) - constants.L0 ) ) )
  Pfric = constants.muk * constants.mass * constants.grav * math.sqrt( vx*vx + vy*vy )
  Efric = Efric + Pfric*h
  Etot  = Emech + Efric
  #  write output line to file
  filename.write( f"{t:.15f} {x:.15f} {y:.15f} {Etot:.15f} {Emech:.15f} {Efric:.15f} {Pfric:.15f}\n" )
filename.close()


