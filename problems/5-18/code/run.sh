#!/bin/bash

prog="5-18"

rm -f soln.txt 5-18-multiplot.png
make clean
#  https://askubuntu.com/a/864301
if [ "yas$1" == "yas" ]; then
#if [ -z "$1" ]; then
  make
else
  make CC="$1"
fi

./$prog
gnuplot ../images/"$prog"-multiplot.gnuplot
display "$prog"-multiplot.png &
