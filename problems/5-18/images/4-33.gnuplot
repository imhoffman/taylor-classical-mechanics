## Taylor 4-33
##  not using the suggested natural units
g = 9.82
m = 0.500
r = 0.10
b = 0.9*r
U(x) = m*g*( (r+b)*cos(x) + r*x*sin(x) )
set samples 10000
set key inside bottom right
##
##  part a
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 18" linewidth 2
set output '4-33.png'
set xzeroaxis
set xtics pi/16.0
set format x '%.3P'
set xlabel 'angle {/Symbol q} from vertical (rad/{/Symbol p})'
set ylabel 'potential energy (J)'
plot [-pi/4.0:+pi/4.0] U(x) title '{/:Italic U}({/Symbol q})' lt rgb "#FF0000"
##
##  part b
#reset
#set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 18" linewidth 2
set output '4-33-tan.png'
set ylabel 'function value'
dUdx1(x) = x
dUdx2(x) = b/r*tan(x)
plot [0:+pi/4.0] dUdx1(x) title 'left-hand side' lt rgb "#00FF00", dUdx2(x) title 'right-hand side' lt rgb "#0000FF"
