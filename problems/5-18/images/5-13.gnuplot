# http://ayapin-film.sakura.ne.jp/Gnuplot/Docs/ps_guide.pdf
#  problem 5-13
#  `every ::1` is for skipping header line
#  `+ 0.0` for casting as a float ... use `+0` for int
filename = "soln.txt"
## parse header
fapprox = system("head -1 " . filename . " | awk '{printf \"%.8f\", $7}'")
fapprox = fapprox + 0.0
U0 = system("head -1 " . filename . " | awk '{printf \"%.8f\", $4}'")
U0 = U0 + 0.0
lambda = system("head -1 " . filename . " | awk '{printf \"%.8f\", $5}'")
lambda = lambda + 0.0
R = system("head -1 " . filename . " | awk '{printf \"%.8f\", $6}'")
R = R + 0.0
##
##  first plot, the potential
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set output '5-13-potential.png'
set samples 10000
U(x) = U0 * ( x/R + lambda*lambda*R/x )
set title '5-13, the potential for the numerical investigation'
set xlabel 'position, {/:Italic r} (m)'
set ylabel 'potential energy, {/:Italic U} (J)'
set label sprintf("{/:Italic U}_0 = %#.3g J\n{/:Italic R} = %#.3g m\n{/Symbol l} = %#.3g\n{/:Italic r}_0 = {/Symbol l}{/:Italic R} = %#.3g m", U0, R, lambda, lambda*R) at graph 0.5,0.85
plot [+0.0:2.0*R] [0.0:+3.0*U(R)] U(x) title '' lc rgb "#FF00FF"
##
##  second plot, r and phi versus time
reset
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set key inside bottom right
#set key font ",18"
#set xtics font ",18"
#set ytics font ",18"
#set y2tics font ",18"
set output '5-13-time.png'
set title sprintf("5-13, expected radial frequency %#.3g Hz", fapprox)
set xzeroaxis
set xlabel 'time, {/:Italic t} (s)'
set ylabel 'radial position of particle, {/:Italic r} (m)' tc lt 1
set ytics nomirror tc lt 1
set yrange [-0.35:+0.35]
set y2label 'angular position of particle, {/Symbol f} (rad)' tc lt 2
set y2tics nomirror tc lt 2
set y2range [-pi:+pi]
#set format y2 "%.0P{/Symbol p}"
set y2tics ( "-{/Symbol p}" -pi, "-{/Symbol p}/2" -pi/2.0, "0" 0.0, "+{/Symbol p}/2" pi/2.0, "+{/Symbol p}" +pi)
plot filename every ::1 using 1:2 with lines title 'numerical radius' axes x1y1 lt 1, filename every ::1 using 1:3 with lines title 'numerical angle' axes x1y2 lt 2
##
##  third plot, parametric spatial path
reset
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set xtics font ",18"
set ytics font ",18"
set key inside top left
set output '5-13-orbit.png'
set xzeroaxis
set yzeroaxis
set size ratio -1
set title '5-13, planar trajectory'
set xlabel '{/:Italic x} position of particle (m)'
set ylabel '{/:Italic y} position of particle (m)'
plot filename every ::1 using 4:5 with lines title '' lc rgb "#FF00FF"
