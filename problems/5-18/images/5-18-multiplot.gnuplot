## 5-18
##  
filename = "soln.txt"
## parse header
a = system("head -1 " . filename . " | awk '{printf \"%.8f\", $3}'")
a = a + 0.0
L0 = system("head -1 " . filename . " | awk '{printf \"%.8f\", $4}'")
L0 = L0 + 0.0
k = system("head -1 " . filename . " | awk '{printf \"%.8f\", $2}'")
k = k + 0.0
m = system("head -1 " . filename . " | awk '{printf \"%.8f\", $1}'")
m = m + 0.0
tmin = system("head -1 " . filename . " | awk '{printf \"%.8f\", $5}'")
tmin = tmin + 0.0
tmax = system("head -1 " . filename . " | awk '{printf \"%.8f\", $6}'")
tmax = tmax + 0.0
muk = system("head -1 " . filename . " | awk '{printf \"%.8f\", $7}'")
muk = muk + 0.0
y0 = system("head -1 " . filename . " | awk '{printf \"%.8f\", $8}'")
y0 = y0 + 0.0
f0x = system("head -1 " . filename . " | awk '{printf \"%.8f\", $9}'")
f0x = f0x + 0.0
f0y = system("head -1 " . filename . " | awk '{printf \"%.8f\", $10}'")
f0y = f0y + 0.0
##  determine sensible y limits
if ( L0 != a ) { ymax = 2.25 * sqrt( abs( L0*L0 - a*a ) ) } else { ymax = a }
ymin = -ymax
##  multiplot setup
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 12" linewidth 2
set output '5-18-multiplot.png'
set size 0.8, 1.0
set multiplot
sizescale = 0.92
##
##  first plot, potential surface
set origin 0.0, 0.5
set size 0.4*sizescale, 0.5
U(x,y) = (1.0/2.0)*k*( sqrt( (x-a)**2 + y**2 ) - L0 )**2 + (1.0/2.0)*k*( sqrt( (x+a)**2 + y**2 ) - L0 )**2
set samples 31, 41
set isosamples 31, 41
#set key inside top right
set xzeroaxis
set yzeroaxis
set xlabel '{/:Italic x} position (m)'
set ylabel '{/:Italic y} position (m)'
set cblabel 'potential energy, {/:Italic U} (J)' rotate by 90
set pm3d at b
splot [-a:+a] [ymin:ymax] U(x,y) with lines title '{/:Italic U}({/:Italic x},{/:Italic y})'
##
##  third plot, x,y trajectory
reset
set origin 0.4, 0.5
set size 0.4*sizescale, 0.5
isosamps = 81
set xlabel '{/:Italic x} position (m)'
set ylabel '{/:Italic y} position (m)'
set samples isosamps, isosamps*floor(ymax/a)
set isosamples isosamps, isosamps*floor(ymax/a)
set pm3d map
splot [-a:+a] [ymin:ymax] U(x,y), filename every ::1 using 2:3:(0):("green") with lines title '' lc variable
##
##  second plot, x v. t   and y v. t
reset
set origin 0.0, 0.0
set size 0.8, 0.5
set key inside bottom right
set title sprintf("5-18: if {/:Italic x},{/:Italic y} << %.3g m then {/:Italic fx}=%#.2g Hz, {/:Italic fy}=%#.2g Hz approx", \
  a, f0x, f0y )
set label sprintf("{/Symbol m}_{/:Italic k} = %#.3g", muk) at graph 0.85,0.85
#   coords spaces are "graph", "screen", "first", and "second"
set label "+a"   at graph -0.04, first +a tc lt 1
set label "-a"   at graph -0.04, first -a tc lt 1
set arrow nohead from graph 1.00, second +y0 to graph 1.02, second +y0 lt 2
set label "+y_0" at graph 1.05, second +y0 tc lt 2
set arrow nohead from graph 1.00, second -y0 to graph 1.02, second -y0 lt 2
set label "-y_0" at graph 1.05, second -y0 tc lt 2
set xzeroaxis
set xlabel 'time, {/:Italic t} (s)'
set ylabel '{/:Italic x} position of particle (m)' tc lt 1
set ytics nomirror tc lt 1
set y2label '{/:Italic y} position of particle (m)' tc lt 2
set y2tics nomirror tc lt 2
set yrange [-a:+a]
set y2range [ymin:ymax]
plot [tmin:tmax] filename every ::1 using 1:2 with lines title 'numerical x' axes x1y1 lt 1, filename every ::1 using 1:3 with lines title 'numerical y' axes x1y2 lt 2
##
##
unset multiplot
set size 1.0, 0.8
