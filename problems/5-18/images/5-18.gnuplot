## 5-18
filename = "soln.txt"
## parse header
a = system("head -1 " . filename . " | awk '{printf \"%.8f\", $3}'")
a = a + 0.0
L0 = system("head -1 " . filename . " | awk '{printf \"%.8f\", $4}'")
L0 = L0 + 0.0
k = system("head -1 " . filename . " | awk '{printf \"%.8f\", $2}'")
k = k + 0.0
m = system("head -1 " . filename . " | awk '{printf \"%.8f\", $1}'")
m = m + 0.0
tmin = system("head -1 " . filename . " | awk '{printf \"%.8f\", $5}'")
tmin = tmin + 0.0
tmax = system("head -1 " . filename . " | awk '{printf \"%.8f\", $6}'")
tmax = tmax + 0.0
##  determine sensible y limits
if ( L0 != a ) {ymax = 2.25 * sqrt( abs( L0*L0 - a*a ) ) } else { ymax = a }
ymin = -ymax
##
##  first plot, potential surface
U(x,y) = (1.0/2.0)*k*( sqrt( (x-a)**2 + y**2 ) - L0 )**2 + (1.0/2.0)*k*( sqrt( (x+a)**2 + y**2 ) - L0 )**2
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 18" linewidth 2
set output '5-18-potential.png'
set samples 31, 41
set isosamples 31, 41
#set key inside top right
set xzeroaxis
set yzeroaxis
set xlabel '{/:Italic x} position (m)'
set ylabel '{/:Italic y} position (m)'
set cblabel 'potential energy, {/:Italic U} (J)' rotate by 90
set pm3d at b
splot [-a:+a] [ymin:ymax] U(x,y) with lines title '{/:Italic U}({/:Italic x},{/:Italic y})'
##
##  second plot, x v. t   and y v. t
reset
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set key inside bottom right
#set key font ",18"
#set xtics font ",18"
#set ytics font ",18"
#set y2tics font ",18"
set output '5-18-time.png'
#   y freq is undefined for L0>a but it should print as 0.00
set encoding utf8
set title sprintf("5-18: if {/:Italic x},{/:Italic y} << %.3g m then {/:Italic fx}=%#.3g Hz, {/:Italic fy}=%#.3g Hz approx", \
  a, \
  sqrt( 2.0 * k / m )/2.0/3.14159, \
  sqrt( 2.0 * k * ( 1.0 - L0/a ) / m )/2.0/3.14159 )
set xzeroaxis
set xlabel 'time, {/:Italic t} (s)'
set ylabel '{/:Italic x} position of particle (m)' tc lt 1
set ytics nomirror tc lt 1
set yrange [ymin:ymax]
set arrow 1 to tmin,+a from 0.05*(tmax-tmin),+a lt 1
set arrow 2 to tmin,-a from 0.05*(tmax-tmin),-a lt 1
set y2label '{/:Italic y} position of particle (m)' tc lt 2
set y2tics nomirror tc lt 2
set y2range [ymin:ymax]
plot filename every ::1 using 1:2 with lines title 'numerical x' axes x1y1 lt 1, filename every ::1 using 1:3 with lines title 'numerical y' axes x1y2 lt 2
##
##  third plot, x,y trajectory
reset
isosamps = 81
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 18" linewidth 2
set output '5-18-xy.png'
set xlabel '{/:Italic x} position (m)'
set ylabel '{/:Italic y} position (m)'
set samples isosamps, isosamps*floor(ymax/a)
set isosamples isosamps, isosamps*floor(ymax/a)
set pm3d map
splot [-a:+a] [ymin:ymax] U(x,y), filename every ::1 using 2:3:(0):("green") with lines title '' lc variable
