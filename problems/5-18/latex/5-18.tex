\documentclass[fleqn,leqno,12pt,letterpaper]{report}

\usepackage[top=2.5cm, bottom=2.5cm, left=2.5cm, right=2.5cm, landscape]{geometry}
\usepackage{amsmath}
\usepackage{mathtools}
\usepackage{cancel}
\usepackage{bigints}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{hyperref}
\usepackage{pdflscape}
\pagestyle{empty}

\newcommand{\der}[2]{\frac{d#1}{d#2}}
\newcommand{\pder}[2]{\frac{\partial#1}{\partial#2}}

\begin{document}

\noindent Lagrangian Mechanics --- Ian M.\ Hoffman --- Spring 2019 Block 3
\bigskip

In solving Problem 5-18, it is valuable for us to first revisit our solutions to a few simpler problems first.
In this document are solutions to 4-33, 5-13, and 5-18 that extend beyond the requested answers into Lagrangian analyses of interesting cases.

\vspace{1cm}
\noindent{\bf Problem 4-33}\\
\begin{wrapfigure}{r}{0.40\textwidth}
    \includegraphics[width=0.39\textwidth]{4-33.png}
  \caption{The potential in Problem~4-33 for $m=0.500$~kg, $r=0.10$~m, and $b=0.09$~m.}
\end{wrapfigure}
From Example 4.7, we are given
\begin{align*}
U(\theta) &= mg\left[ \left(r+b\right)\cos\theta + r\theta\sin\theta \right] \\[1em]
\frac{dU}{d\theta} &= mg\left[ r\theta\cos\theta -b\sin\theta\right] \\[1em]
\frac{d^2U}{d\theta^2} &= mg\left[ r-b \right]\ .
\end{align*}
Contrary to the suggestion in the problem for choice of units, I have chosen useful values for $m$ and $r$ (0.5~kg and 10~cm) and used the SI $g$ so that my vertical axis has unit of Joules.
Plots of the potential and of the graphical transcendental solution (along with the associated gnuplot code) can be found in the repository.

As expected, the equilibrium at the vertical ($\theta=0$) balance point is stable for $r>b$ and unstable for $r<b$.
However, the stable case is not stable for very large displacements from the vertical.
This is not surprising; surely the cylinder cannot recover from $90^\circ$ and so there must be a turnover in the function somewhere $0<\theta<\pi/2$.
In examining the first derivative
\[
\frac{d}{d\theta}U = mg\left[ r\theta\cos\theta - b\sin\theta \right]\ ,
\]
we find that it is zero at both $\theta = 0$ and when 
\[
\theta = \frac{b\cos\theta}{r\sin\theta} = \frac{b}{r}\tan\theta\ .
\]
This is transcendental but can be found graphically to be satisfied for approximately $\theta = 0.173\pi$ when $b=9$~cm.
That is, $b<r$ and so there is a stable equilibrium at $\theta=0$ and a second equilibrium farther out.
Mathematically, the second equilibrium is seen to be unstable based on the sign of the second derivative (and/or by reasoning or inspecting the curvature of the plot).
Physically, the equilibrium is expected to be unstable based on a torques analysis.
Noting that the angle in Figure~4.14 at point $C$ between the dotted line and the line to point $B$ is $\theta$ and that the distance from point $B$ to point $C$ is the rolled arc length $r\theta$, the lever arm (i.e., the horizontal distance from point $O$) of the weight of the block is
\[
(r+b)\sin\theta - r\theta\cos\theta\ .
\]
The lever arm of the normal force up on the block is $r\sin\theta$.
The sum of the torques is
\begin{align*}
 & -mg\left[(r+b)\sin\theta - r\theta\cos\theta\right] + mg\left[r\sin\theta\right] \\[1em]
=& mg\left[-b\sin\theta + r\theta\cos\theta\right]\ .
\end{align*}
In other words, once the center of mass rolls out beyond the contact point, the block unstably tips off of the cylinder.
Small amounts of rolling from the vertical that do not pass this boundary result in recovery toward $\theta=0$.

\clearpage
\noindent{\bf Problem 5-13}\\
\begin{wrapfigure}{r}{0.40\textwidth}
    \includegraphics[width=0.45\textwidth]{5-13-potential.png}
  %\caption{The potential in Problem~5-13.}
\end{wrapfigure}
For the potential 
\[
U = U_0\left(\frac{r}{R}+\lambda^2\frac{R}{r}\right)\ ,
\]
the first derivative is
\[
\frac{d}{dr}U = U_0\left(\frac{1}{R} -\lambda^2\frac{R}{r^2}\right)\ .
\]
At equilibrium, this slope is zero and we set $r=r_0$ such that
\begin{align*}      0 &= U_0\left(\frac{1}{R} -\lambda^2\frac{R}{r_0^2}\right) \\[1em] \frac{1}{R} &= \lambda^2\frac{R}{r_0^2} \\[1em] r_0 &= \pm\lambda R\ ,          \end{align*}
where we need only keep the positive root since this is a radial potential.

The second derivative is
\[
\frac{d^2}{dr^2}U = U_0\left(2\lambda^2\frac{R}{r^3}\right)\ ,
\]
from which we may construct the Taylor expansion of $U$ about $r=r_0$ as
\begin{align*} U(r=r_0) &= \frac{1}{0!}\left.U\right|_{r_0}(r-r_0)^0 \quad+\quad \frac{1}{1!}\left.\frac{d}{dr}U\right|_{r_0}(r-r_0)^1 \quad+\quad \frac{1}{2!}\left.\frac{d^2}{dr^2}U\right|_{r_0}(r-r_0)^2 \quad+\quad \cdots \\[1em] &\approx \text{constant} \qquad+\qquad 0 \qquad+\qquad \frac{1}{2}\left[ 2U_0\lambda^2\frac{R}{r_0^3} \right](r-r_0)^2\ ,          \end{align*}
which has a force constant of
\[
k = \frac{2U_0}{\lambda R^2}\ ,
\]
with units of force per length, as expected.

Alternatively, we may find this answer without a Taylor expansion.
In the book it is requested that we expand using $x=r-r_0$.
For this, we can use the function $U$ (without any of its derivatives) along with $r=x+r_0$ substituted; we examine the expression for small $x$:
\begin{align*} U &= U_0\left(\frac{x+r_0}{R}+\lambda^2\frac{R}{x+r_0}\right) \\[1em] &= U_0\left(\lambda\frac{x+r_0}{r_0} + \lambda\frac{r_0}{x+r_0}\right) \\[1em] &=  U_0\lambda\left(\frac{x}{r_0} + \frac{r_0}{r_0} + \frac{1}{x/r_0+1}\right) \\[1em] &= U_0\lambda\left(\frac{x}{r_0} + 1 + \left[1+\frac{x}{r_0}\right]^{-1}\right) \\[1em] &= U_0\lambda\left(\frac{x}{r_0} + 1 + \left[1-\frac{x}{r_0}+\frac{x^2}{r_0^2}-\cdots\right]\right) \\[1em] &\approx U_0\lambda\left(2+\frac{x^2}{r_0^2}\right) \\[1em] &= U_0\lambda\frac{x^2}{\lambda^2R^2} + 2U_0\lambda \\[1em] &= \frac{1}{2}\left(\frac{2U_0}{\lambda R^2}\right)x^2 + \text{constant}\ ,
\end{align*}
which arrives at the same value for $k$.
In this case, the approximation entered when using the binomial expansion to express the smallness of the displacements from equilibrium.

For either method, the frequency of oscillations is
\[
\omega = \sqrt{\frac{k}{m}} = \sqrt{\frac{2U_0}{m\lambda R^2}}\ .
\]

In order to confirm the expected frequency, we may apply some Chapter 7 techniques are generate a numerical solution to the problem.
In two dimensions (for motion in a plane in the radial potential), the Lagrangian is
\[
\mathcal{L} = \frac{1}{2}m\left(\dot{x}^2+\dot{y}^2\right) - U_0\left(\frac{\sqrt{x^2+y^2}}{R}+\lambda^2\frac{R}{\sqrt{x^2+y^2}}\right)\ ,
\]
or
\[
\mathcal{L} = \frac{1}{2}m\left(\dot{r}^2+r^2\dot{\phi}^2\right) - U_0\left(\frac{r}{R}+\lambda^2\frac{R}{r}\right)\ .
\]
The equations of motion in polar coordinates are
\begin{align*}
\frac{\partial\mathcal{L}}{\partial r} &= mr\dot{\phi}^2 - U_0\left(\frac{1}{R} - \lambda^2\frac{R}{r^2}\right) \\[1em]
\frac{\partial\mathcal{L}}{\partial \dot{r}} &= m\dot{r} \\[1em]
\frac{d}{dt}\frac{\partial\mathcal{L}}{\partial \dot{r}} &= m\ddot{r}  \\[1em]
m\ddot{r} &= mr\dot{\phi}^2 - U_0\left(\frac{1}{R} - \lambda^2\frac{R}{r^2}\right)\ ,  \\[2em]
\frac{\partial\mathcal{L}}{\partial \phi} &= 0 \\[1em]
\frac{\partial\mathcal{L}}{\partial \dot{\phi}} &= mr^2\dot{\phi}   \\[1em]
\frac{d}{dt}\frac{\partial\mathcal{L}}{\partial \dot{\phi}} &= mr^2\ddot{\phi}  \\[1em]
mr^2\ddot{\phi} &= 0 \\[1em]
mr^2\dot{\phi} &= \text{constant}\ .
\end{align*}


For the following arbitrary values
\begin{align*}
m &= 0.250\,\text{kg} \\
U_0 &= 12\,\text{J} \\
\lambda &= 0.75 \\
R &= 25\,\text{cm}\,
\end{align*}
the expected frequency of oscillation about $r_0 = R$ is
\begin{align*}
\omega &= \sqrt{\frac{2U_0}{m\lambda R^2}} = 45.25\,\text{rad/s} \\[1em]
f &= \frac{\omega}{2\pi} = 7.20\,\text{Hz}\ .
\end{align*}
The code and plots in the repository are in good agreement with this expectation.
\begin{figure}[h]
  \centering
    \includegraphics[width=0.55\textwidth, angle=000]{5-13-time.png}~~~\includegraphics[width=0.35\textwidth]{5-13-orbit.png}
     %\caption{blah}
\end{figure}

\clearpage
\noindent{\bf Problem 5-18}\\

\begin{figure}[h]
  \centering
    \includegraphics[width=0.55\textwidth, angle=000]{Fig-5-27.png}
     \caption{Note that this is a {\it top-down view} of a mass that is free to move in two horizontal directions. We have chosen $x$ to be horizontal in the image and $x=0$ at the location pictured (such that the walls lie at $x=+a$ and $x=-a$); $y$ is vertical on the page and $y=0$ and the location pictured.}
\end{figure}


\vspace{1cm}\noindent{\it Writing out the potential---}\\
The potential energy stored in the springs is
\[
U(x,y) = \frac{1}{2}k\left(\sqrt{(a-x)^2+y^2}-\ell_0\right)^2 + \frac{1}{2}k\left(\sqrt{(a+x)^2+y^2}-\ell_0\right)^2\ .
\]
The goal is to make this look like
\[
U(x,y) = \frac{1}{2}\left(k_x x^2 + k_y y^2\right)\ ,
\]
where the $x$ and $y$ dependencies have been decoupled and have been associated with different constants than those of the springs.
Indeed, for small departures from the center of the apparatus, we expect no $x$ dependence on the $y$ position and vice-versa; so we examine the regime in which both $x \ll a$ and $y \ll a$.

\vspace{1cm}\noindent{\it Envisioning the approximations---}\\
Since $x$ and $y$ are coupled by the square-root function, we can attack there.
Breaking up that one problematic aspect of the expression is reminiscent of how we handled Problem~5-13; rather than write out the entire $U$ function and then Taylor-expanding the entire thing for analysis, we simply rewrite the one or two problematic terms.

\vspace{1cm}\noindent{\it Executing the approximations---}\\
We shall unpack the square-root using the Taylor expansion: for example, as shown on \href{https://en.wikipedia.org/wiki/Square\_root#Properties\_and\_uses}{this Wikipedia page}
\[
\sqrt{1 + \epsilon} = \sum_{n=0}^\infty \frac{(-1)^n(2n)!}{(1-2n)(n!)^2(4^n)}\epsilon^n = 1 + \frac{1}{2}\epsilon - \frac{1}{8}\epsilon^2 + \frac{1}{16} \epsilon^3 - \frac{5}{128} \epsilon^4 + \cdots,
\]
where I have used $\epsilon$, as in class.
In order to obtain an $\epsilon$ that is sufficiently small, we factor out an $a^2$ in order to obtain the small values $x/a$ and $y/a$.
Then, we shall keep whatever terms allow us to continue to describe the oscillatory potential---that is, quadratic.
With $a$ factored out, we have:
\begin{align*}
U(x,y) &= \frac{1}{2}ka^2\left(\sqrt{\frac{x^2}{a^2}-2\frac{x}{a}+1+\frac{y^2}{a^2}}-\frac{\ell_0}{a}\right)^2 + \frac{1}{2}ka^2\left(\sqrt{\frac{x^2}{a^2}+2\frac{x}{a}+1+\frac{y^2}{a^2}}-\frac{\ell_0}{a}\right)^2 \\[1em]
\end{align*}
In our case, we are interested in approximating
\begin{align*}
\sqrt{1 + \epsilon} &= \sqrt{1+\underbrace{\frac{x^2}{a^2}-2\frac{x}{a}+\frac{y^2}{a^2}}_\epsilon} \\[1em]
 &= 1 + \frac{1}{2}\frac{x^2}{a^2}-\frac{1}{2}2\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2} - \frac{1}{8}\left(-2\frac{x}{a}\right)^2 + \mathcal{O}(x^3,y^3) \\[1em]
 &\approx 1 -\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}\ .
\end{align*}
Applying this same expansion to the other term for the other spring, our $U$ is now
\begin{align*}
U(x,y) &\approx \frac{1}{2}ka^2\left(1 -\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a}\right)^2 + \frac{1}{2}ka^2\left(1 +\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a}\right)^2 \\[1em]
&= \frac{1}{2}ka^2\left[ 1 -\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a} - \frac{x}{a}\left(1 -\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a}\right) + \frac{1}{2}\frac{y^2}{a^2}\left(1 -\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a}\right) -\frac{\ell_0}{a}\left(1 -\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a}\right)\right. \\[1em]
&\qquad \left. + 1 +\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a} + \frac{x}{a}\left(1 +\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a}\right) + \frac{1}{2}\frac{y^2}{a^2}\left(1 +\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a}\right) -\frac{\ell_0}{a}\left(1 +\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a}\right) \right] \\[1em]
&= \frac{1}{2}ka^2\left[ 1 -\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a} - \frac{x}{a} +\frac{x^2}{a^2} - \frac{x}{a}\frac{1}{2}\frac{y^2}{a^2} + \frac{x}{a}\frac{\ell_0}{a} + \frac{1}{2}\frac{y^2}{a^2} -\frac{1}{2}\frac{y^2}{a^2}\frac{x}{a} +\frac{1}{2}\frac{y^2}{a^2}\frac{1}{2}\frac{y^2}{a^2} -\frac{1}{2}\frac{y^2}{a^2}\frac{\ell_0}{a} -\frac{\ell_0}{a} +\frac{\ell_0}{a}\frac{x}{a} -\frac{\ell_0}{a}\frac{1}{2}\frac{y^2}{a^2} +\frac{\ell_0}{a}\frac{\ell_0}{a} \right. \\[1em]
&\qquad \left. + 1 +\frac{x}{a}+\frac{1}{2}\frac{y^2}{a^2}-\frac{\ell_0}{a} + \frac{x}{a} +\frac{x}{a}\frac{x}{a} +\frac{x}{a}\frac{1}{2}\frac{y^2}{a^2} -\frac{x}{a}\frac{\ell_0}{a} + \frac{1}{2}\frac{y^2}{a^2} +\frac{1}{2}\frac{y^2}{a^2}\frac{x}{a} +\frac{1}{2}\frac{y^2}{a^2}\frac{1}{2}\frac{y^2}{a^2} -\frac{1}{2}\frac{y^2}{a^2}\frac{\ell_0}{a} -\frac{\ell_0}{a} -\frac{\ell_0}{a}\frac{x}{a} -\frac{\ell_0}{a}\frac{1}{2}\frac{y^2}{a^2} +\frac{\ell_0}{a}\frac{\ell_0}{a}   \right] \\[1em]
&= \frac{1}{2}ka^2\left[ 1 + 4\frac{1}{2}\frac{y^2}{a^2} - 4\frac{\ell_0}{a} + 2\frac{x^2}{a^2}  + \cancelto{\mathcal{O}(y^4)}{ \frac{1}{2}\frac{y^2}{a^2}\frac{y^2}{a^2} } - 4\frac{1}{2}\frac{y^2}{a^2}\frac{\ell_0}{a} + 2\frac{\ell_0}{a}\frac{\ell_0}{a} + 1  \right] \\[1em]
&\approx \frac{1}{2}ka^2\left[ 2\frac{x^2}{a^2} + 2\frac{y^2}{a^2}\left( 1 - \frac{\ell_0}{a} \right) + 2\left( 1 - 2\frac{\ell_0}{a} + \frac{\ell_0^2}{a^2} \right)  \right]\ .
\end{align*}

\vspace{1cm}\noindent{\it Analyzing the approximated potential---}\\
This wouldn't be a three-star problem if we hadn't been faced with some tough decisions on how to collect all of the terms in the approximated expression for $U$.
As a {\bf first priority}, we collected $x^2$ and $y^2$ so that we can assemble our desired $k_x$ and $k_y$ terms.

As a {\bf second priority}, we are tasked with considering the cases of $a>\ell_0$ and $a<\ell_0$ in terms of how the sign of the second derivative is affected.
We have encountered this sort of analysis before---for example, in Problem 4-33.
In that cube-on-cylinder problem, we were comparing differences of two lengths: $(r-b)$.
In the present problem, the analogous test would be on $(a-\ell_0)$.
However, in our analysis of the cube-on-cylinder, we examined the transcendental solutions to the other critical points using the ratio $b/r$ rather than the difference $(r-b)$.
This technique of studying the ratio is pertinent here.
Indeed, since we factored out $a$ in order to facilitate our expansion of the square-root, we are already set up to analyze $\left(1-\frac{\ell_0}{a}\right)$ rather than $(a-\ell_0)$; so, we collect $\left(1-\frac{\ell_0}{a}\right)$.

Without any further approximations, the preceding expression may be finally rewritten as
\[
U(x,y) = kx^2 + k\left(1-\frac{\ell_0}{a}\right)y^2 + \underbrace{k\left(1-\frac{\ell_0}{a}\right)^2a^2}_\text{constants}\ ,
\]
where we have succeeded in collecting both $x^2$ and $y^2$ as well as $\left(1-\frac{\ell_0}{a}\right)$.
The final term is an nonphysical constant offset that---by now---we are used to ignoring.
Once the constants are ignored, we can complete {\bf our first task}, which is to make the comparison
\[
\begin{array}{lcrcr}
U(x,y) &=& \displaystyle\frac{1}{2}k_x x^2 &+& \displaystyle\frac{1}{2}k_y y^2 \\[1em]
       &=& kx^2 &+& k\left(1-\frac{\ell_0}{a}\right)y^2
\end{array}
\]
so that we may recognize what the author intends by $k_x$ and $k_y$: namely,
\begin{align*}
k_x &= 2k \\[1em]
k_y &= 2k\left(1-\frac{\ell_0}{a}\right)\ .
\end{align*}
For the $x$ direction, the curvature of the parabola makes sense: it is a positive value that is equal to two spring's worth of restoring force.
Indeed, in the $x$ direction, the mass oscillates by being restored by two springs.

It is the $y$ direction that is more interesting.
There are three cases for the comparison of $a$ and $\ell_0$:
\begin{enumerate}
\item When $a>\ell_0$, the strings are already stretched at $y=0$ and displacements of $y$ only serve to stretch them more. In other words, we are already experiencing a great deal of Hooke's Law restoring force and the $y$ parabola is quite taut.
If $a\gg\ell_0$ then the mass is restored in $y$ as it is in $x$; the spring is so taut that the tiny perpendicular component has grown to be comparable to $kx$---this is the complement of the familiar introductory stump-pulling problem.\footnote{\url{https://www.wired.com/2016/12/pull-car-ditch-super-strength-physics/}}
\item If $a=\ell_0$, the restoring force in the $y$ direction is zero. Indeed, if the springs are not yet stretched, a tiny displacement is not going to offer much restoring force. In this case, the landscape is flat in the $y$ direction and the mass feels nothing when displaced in $y$.
\item Considering $a<\ell_0$ is {\bf our second task}: to note that in the $y$ direction the mass will not oscillate stably about the center. If $\ell_0>a$, then $\left(1-\ell_0/a\right)<0$ and the potential parabola is upside-down---the equilibrium is not stable.
The sign $\left(a-\ell_0\right)<0$ means that the springs are in compression and susceptible to buckling (although remaining in the horizontal plane).
\end{enumerate}

In the following section, a complete numerical evolution of the system is described.
We may test our approximated expectations for the $a>\ell_0$ case by providing small-displacement boundary conditions to the numerical algorithm and then inspecting the trajectories for simultaneous, independent oscillations at frequencies of
\begin{align*}
f_x &= \frac{1}{2\pi}\sqrt{\frac{2k}{m}}\\[1em]
f_y &= \frac{1}{2\pi}\sqrt{\frac{2k\left(1-\ell_0/a\right)}{m}}\ .
\end{align*}
As a preview, shown in Fig.~\ref{approx} is a plot of the $x$ and $y$ motion for conditions that satisfy the approximation.
\begin{figure}[h]
 \centering
    \includegraphics[width=0.45\textwidth]{5-18-time-approx.png}~~~
    \includegraphics[width=0.45\textwidth]{5-18-xy-approx.png}
  \caption{The $x$ and $y$ motion in Problem~5-18 for $m=65$~g, $a=90$~cm, $\ell_0=80$~cm, and $k_1=k_2=5$~N/m when the bob is released from rest at ($+16$~cm,$-11$~cm).
The color scale is potential energy in Joules---small displacements are needed in order to avoid the structure that is apparent away from the origin.\label{approx}}
\end{figure}

\vspace{1cm}\noindent{\it Numerical analysis of the non-approximated potential---}\\
For $a<\ell_0$, we expect that there must be symmetrically placed stable equilibria at non-zero values of $y$ toward which the mass will move upon buckling.
The locations of the equilibria $y=y_0$ will satisfy
\begin{align*}
\ell_0^2 &= y_0^2 + a^2\\[1em]
y_0 &= \sqrt{\ell_0^2 - a^2}\ .
\end{align*}
For the values $a=0.9$, $\ell_0=1.1$, and $k=5.0$ we expect equilibria at $y$ values of
\[
y_0 = \sqrt{(1.1\,\text{m})^2 - (0.9\,\text{m})^2} = \pm 0.632\,\text{m}\ ,
\]
in good agreement with the plot in Fig.~\ref{buckling}.
For these values, our assumption of $y \ll a$ is violated and we need a full, non-approximated analysis.\footnote{It is left as an exercise for the reader to expand the $a<\ell_0$ potential about $y_0$ in order to form an expectation for the frequency of small-displacement oscillations about $y_0$.}

In pursuit of the equations of motion, we might either sum the forces or embark on a Chapter-7 style Lagrangian analysis.
Let us attempt the latter in the hopes that it reconciles with the former.
Our Lagrangian is
\[
\mathcal{L} = \frac{1}{2}\left( \dot{x}^2 + \dot{y}^2 \right) - \frac{1}{2}k\left(\underbrace{\sqrt{(a+x)^2+y^2}}_{\beta_1}-\ell_0\right)^2 - \frac{1}{2}k\left(\underbrace{\sqrt{(a-x)^2+y^2}}_{\beta_2}-\ell_0\right)^2\ ,
\]
where the expressions for $\beta_1$ and $\beta_2$ are shorthands used in the codes.
The equations of motion are
\begin{align*}
m\ddot{x} &= k\left[\left(\sqrt{(a-x)^2+y^2}-\ell_0\right)\frac{a-x}{\sqrt{(a-x)^2+y^2}} - \left(\sqrt{(a+x)^2+y^2}-\ell_0\right)\frac{a+x}{\sqrt{(a+x)^2+y^2}} \right] \\[1em]
m\ddot{y} &= -k\left[\left(\sqrt{(a-x)^2+y^2}-\ell_0\right)\frac{y}{\sqrt{(a-x)^2+y^2}} + \left(\sqrt{(a+x)^2+y^2}-\ell_0\right)\frac{y}{\sqrt{(a+x)^2+y^2}} \right]\ .
\end{align*}
We do not solve these equations analytically; the numerical code in the repository uses these expressions and may be seen in action in Fig.~\ref{buckling}.
\begin{figure}[h]
 \centering
    \includegraphics[width=0.45\textwidth]{5-18-time.png}~~~
    \includegraphics[width=0.45\textwidth]{5-18-xy.png}
  \caption{The $x$ and $y$ motion in Problem~5-18 for $m=65$~g, $a=90$~cm, $\ell_0=110$~cm, and $k_1=k_2=5$~N/m when the bob is released from rest at ($+21$~cm,$-59$~cm).
The color scale is potential energy in Joules.\label{buckling}}
\end{figure}
As we had hoped, our expressions for $m\ddot{x}$ and $m\ddot{y}$ may be readily recognized as the forces from the springs.
The extensions, $s$, of the left and right springs are
\begin{align*}
s_\ell &= \sqrt{(a+x)^2+y^2}-\ell_0 \\[1em]
s_r    &= \sqrt{(a-x)^2+y^2}-\ell_0\ ,
\end{align*}
and factors such as
\[
\frac{a-x}{\sqrt{(a-x)^2+y^2}}
\]
are the trigonometric scalings of the $x$ and $y$ components.
With the forces laid out before us, we might continue our bonus analysis of this problem by adding some drag to the mass along the table.
In this case, the drag will be in the form of $\mu_k m g$ kinetic friction.
Similar to the use of $\hat{v}$ in Equation~2.60 in the run-up to Example~2-6, we use the components of the velocity vector in order to determine the direction in which friction points.
Thus, the equations of motion become
\begin{align*}
m\ddot{x} &= k\left[\left(\sqrt{(a-x)^2+y^2}-\ell_0\right)\frac{a-x}{\sqrt{(a-x)^2+y^2}} - \left(\sqrt{(a+x)^2+y^2}-\ell_0\right)\frac{a+x}{\sqrt{(a+x)^2+y^2}} \right] - \mu_kmg\frac{v_x}{\sqrt{v_x^2+v_y^2}} \\[1em]
m\ddot{y} &= -k\left[\left(\sqrt{(a-x)^2+y^2}-\ell_0\right)\frac{y}{\sqrt{(a-x)^2+y^2}} + \left(\sqrt{(a+x)^2+y^2}-\ell_0\right)\frac{y}{\sqrt{(a+x)^2+y^2}} \right] - \mu_kmg\frac{v_y}{\sqrt{v_x^2+v_y^2}} \ .
\end{align*}
These can be seen in action by setting the value of $\mu_k$ to be nonzero in the accompanying numerical codes.

\end{document}

