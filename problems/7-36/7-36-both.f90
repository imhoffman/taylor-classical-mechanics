
!!
!! parameters and types
!!
  module types
   use defs
   implicit none
   integer, parameter :: Nsteps = 8192     ! number of steps
   integer, parameter :: ndim   = 2        ! 2-D: r, phi
   integer, parameter :: nr = 1, nphi = 4  ! see latex pdf
   real(kind=rw), parameter :: g = 9.82_rw, k = 10.0_rw, L0 = 0.20_rw
   real(kind=rw), parameter :: a = real(nr*nr,kind=rw)/real(nphi*nphi,kind=rw)
   real(kind=rw), parameter :: m = a/(1.0_rw-a)*k*L0/g
   real(kind=rw), parameter :: pi = 3.14159265358979323846264338327950_rw
  end module types

!!
!! subprograms
!!
  module subs
   use defs
   use types
   implicit none
   contains

   !! portable Y for use with RK4/Verlet
   !! the problem at hand: xv = ( r, rdot, phi, phidot )
   pure function Y( xv, rk4bool ) result ( vec_out )
    real(kind=rw), intent(in)  :: xv(:)
    logical, intent(in)        :: rk4bool
    real(kind=rw), allocatable :: x(:), v(:), a(:), vec_out(:)
    integer                    :: ndim

    ndim = size(xv)/2

    allocate( x(ndim), v(ndim), a(ndim) )
    if ( rk4bool ) then
      allocate( vec_out(ndim*2) )
    else
      allocate( vec_out( ndim ) )
    endif

    !!  unpack xv into x and v
    x = xv(1::2)
    v = xv(2::2)

    !!  determine accel
    a(1) = x(1)*v(2)*v(2)  +  g*cos( x(2) ) - k/m*( x(1) - L0 )
    a(2) = (-1.0_rw/x(1))*( 2.0_rw*v(1)*v(2) + g*sin( x(2) ) )

    !!  load up function `result` as per RK4/Verlet
    if ( rk4bool ) then
      vec_out(1::2) = v
      vec_out(2::2) = a
    else
      vec_out = a
    endif

    deallocate( x, v, a )
    !!  `vec_out` deallocates upon return
    return
   end function Y
  end module subs

!!
!! main
!!
  program main
    use defs; use types; use subs; use verlet; use rk4;
    implicit none   
    real(kind=rw)    :: acc_old(ndim), acc_new(ndim)
    real(kind=rw)    :: xv(ndim*2), nil(ndim*2)
    real(kind=rw)    :: r, phi, t, ti, tf, h, L = L0 + m*g/k
    character(len=1) :: env_var
    logical          :: use_rk4

    !! boundary conditions
    ti    =  0.0_rw
    tf    = 10.0_rw
    xv(1) = +1.050_rw * L  !! soln excludes r=0
    xv(2) = +0.00_rw       !! released from rest
    xv(3) = +pi/32.0_rw 
    xv(4) = +0.000_rw      !! released from rest

    !!  default `recl` record length is 40 characters
    !!   per line; pgfortran will enforce!
    open(10,recl=180,file="soln.txt")

    !!  determine method from user's env var
    call get_environment_variable( "SOLN_BY_VERLET", env_var )
    if ( env_var .eq. ' ' ) then
      use_rk4 = .true.
      write(6,*) ' Using Runge--Kutta method.'
    else
      use_rk4 = .false.
      write(6,*) ' Using Verlet method.'
      !!  manually obtain initial Verlet accelerations
      acc_new = Y( xv, use_rk4 )
    endif

    !!
    !!  solution loop
    !!
    h = ( tf - ti ) / real(Nsteps,kind=rw)
    t = ti
    do while ( t .lt. tf )        !! i.e., do i = 1, Nsteps

     !!  update as per env var
     if ( use_rk4 ) then
       call rk4step( h, xv, Y )
     else
       acc_old = acc_new
       call verlet_step( h, xv, acc_old, acc_new, Y )
     endif

     !!  file output
     r   = xv(1)
     phi = xv(3)
     t   = t + h    !!  update time stamp
     write(10,*)  t, r-L, phi, r*sin(phi), -(r*cos(phi)-L)
    end do
    close(10)

    !!  some terminal output
    write(6,'(A,F8.5,A)') ' bob mass: ', m, ' kg;'
    write(6,'(A,F6.2,A,F6.2,A)')         &
     & ' natural pendulum period: ',     sqrt(g/L)/real(2.0,kind=rw)/pi, &
     & ' Hz; natural spring period: ',   sqrt(k/m)/real(2.0,kind=rw)/pi, ' Hz'

    !stop
  end program main

