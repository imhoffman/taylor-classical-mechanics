# http://ayapin-film.sakura.ne.jp/Gnuplot/Docs/ps_guide.pdf
##
##  first plot, r and phi versus time
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set key inside top left
#set key font ",18"
#set xtics font ",18"
#set ytics font ",18"
#set y2tics font ",18"
set output 'gnuplot3.png'
set xzeroaxis
set xlabel 'time, {/:Italic t} (s)'
set ylabel 'total energy (J), RK4 method' tc lt 1
ymax = -0.2770
ymin = -0.2785
set ytics nomirror tc lt 1
#set yrange [ymin:ymax]
set y2label 'total energy (J), Verlet method' tc lt 2
set y2tics nomirror tc lt 2
#set y2range [ymin:ymax]
plot 'RK4.txt' using 1:6 with lines title 'Runge--Kutta' axes x1y1 lt 1, 'Verlet.txt' using 1:6 with lines title 'Verlet' axes x1y2 lt 2
