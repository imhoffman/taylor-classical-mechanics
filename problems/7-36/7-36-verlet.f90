!!
!! parameters and types
!!
  module types_verlet
   use defs
   implicit none
   integer, parameter :: Nsteps = 8192     ! number of steps
   integer, parameter :: ndim   = 2        ! 2-D: r, phi
   integer, parameter :: nr = 1, nphi = 4  ! see latex pdf
   real(kind=rw), parameter :: g = 9.82D0, k = 1.000D1, L0 = 0.20D0
   real(kind=rw), parameter :: a = real(nr*nr,kind=rw)/real(nphi*nphi,kind=rw)
   real(kind=rw), parameter :: m = a/(1.0D0-a)*k*L0/g
   real(kind=rw), parameter :: pi = 3.1415926535897932384626433832795028841971D0
  end module types_verlet
!!
!! subprograms
!!
  module subs_verlet
   use defs
   use types_verlet
   implicit none

   contains

   ! given position, vel, and acc, return new acc
   !  the problem at hand: x = ( r, phi ), v = ( rdot, phidot )
   pure subroutine forces ( x, v, a )
    ! these certainly have length `ndim` but these  `(:)`
    ! declarations need to match the interface in verlet.mod
    real(kind=rw), intent(in)    :: x(:), v(:)
    real(kind=rw), intent(inout) :: a(:)

    a(1) = x(1)*v(2)*v(2)  +  g*cos( x(2) ) - k/m*( x(1) - L0 )
    a(2) = (-1.0D0/x(1))*( 2.0D0*v(1)*v(2) + g*sin( x(2) ) )

    return
   end subroutine forces

  end module subs_verlet
!!
!! main
!!
  program main
    use defs; use types_verlet; use subs_verlet; use verlet
    implicit none   
    real(kind=rw) :: pos(ndim), vel(ndim), acc_old(ndim), acc_new(ndim)
    real(kind=rw) :: r, phi, t, ti, tf, h, L = L0 + m*g/k
    integer :: i 

    ! boundary conditions
    ti     = real(       0.0D0,    kind=rw )
    tf     = real(      10.0D0,    kind=rw )
    pos(1) = L * real(  +1.050,    kind=rw )     ! soln excludes r=0
    vel(1) = real(      +0.00D0,   kind=rw )     ! released from rest
    pos(2) = real(  +pi/32.00D0,   kind=rw ) 
    vel(2) = real(      +0.000D0,  kind=rw )     ! released from rest

    h = ( tf - ti ) / real(Nsteps,kind=rw)

    !  default `recl` record length is 40 characters
    !  per line; pgfortran will enforce!
    open(10,recl=180,file="soln.txt")

    !  manually obtain initial Verlet accelerations
    !  return `new`; `old` is set to `new` before next call
    call forces( pos, vel, acc_new )
    t = ti
    do while ( t .lt. tf )        ! i.e., do i = 1, Nsteps
     ! solution update
     acc_old = acc_new            ! recycle prev computation of acc
     ! obtain new pos and vel
     call verlet_step( h, pos, vel, acc_old, acc_new, forces )
     ! values for data file
     r   = pos(1)
     phi = pos(2)
     t   = t + h                  ! update time stamp
     write(10,*)  t, r-L, phi, r*sin(phi), -(r*cos(phi)-L)
    end do
    close(10)

    !  some terminal output
    write(6,'(A,F8.5,A)') ' bob mass: ', m, ' kg;'
    write(6,'(A,F6.2,A,F6.2,A)')         &
     & ' natural pendulum period: ',     sqrt(g/L)/real(2.0,kind=rw)/pi, &
     & ' Hz; natural spring period: ',   sqrt(k/m)/real(2.0,kind=rw)/pi, ' Hz'

    !stop    ! pgfortran enforces IEEE_INEXACT upon `stop` ...
  end program main

