(require '[clojure.string :as str])
(load-file "../../utilities/taylor.clj")

;;  https://clojure.org/guides/learn/hashed_colls#_looking_up_by_key
;;  https://clojure.org/reference/reader#_literals
(def constants {
   :pi      3.1415926535897932384626433832795028841971
   :nsteps  8192
   :g       9.82	;; gravity (m/s/s)
   :mbob    0.150	;; mass of bob (kg)
   :kspring 95.00	;; stiffness of spring (N/m)
   :L0      0.20	;; length of pendulum (m)
   } )


;;  diff eq system for RK4
(defn Y [f]
  (vector
    (f 1)        ;; r-dot
    (+           ;; r-ddot
      (* (f 0) (f 3) (f 3))
      (-
       (* (constants :g) (Math/cos (f 2)))
       (/ (* (constants :kspring) (- (f 0) (constants :L0)))
          (constants :mbob))))
    (f 3)        ;; phi-dot
    (/           ;; phi-ddot
      (+ (* 2.0 (f 1) (f 3))
         (* (constants :g) (Math/sin (f 2))))
      (* -1.0 (f 0)))))


;;  forces/acceleration for computation for Verlet
;;   input pos, vel, and acc vectors for force computation
;;   return new acc vector
(defn forces [pos vel acc]
  (vector
    (+                         ;; r-ddot
      (* (pos 0) (vel 1) (vel 1))
      (-
       (* (constants :g) (Math/cos (pos 1)))
       (/ (* (constants :kspring) (- (pos 0) (constants :L0)))
          (constants :mbob))))
    (/                         ;; phi-ddot
      (+ (* 2.0 (vel 0) (vel 1))
         (* (constants :g) (Math/sin (pos 1))))
      (* -1.0 (pos 0)))))


;;  compute the total mechnical energy for plotting
;;   PE zero at the pivot
(defn energy [f]
  (let [r      (f 0)
        rdot   (f 1)
        phi    (f 2)
        phidot (f 3)]
    (+
     (* 0.5                      ;; kinetic
        (constants :mbob)
        (+
         (* rdot rdot)
         (* r r phidot phidot)))
     (* -1.0                     ;; grav PE
        (constants :mbob)
        (constants :g)
        r
        (Math/cos phi))
     (* 0.5                      ;; spring PE
        (constants :kspring)
        (*
         (- r (constants :L0))
         (- r (constants :L0)))))))


;;  read environment variable for soln type
;;   either
;;   $ export SOLN_BY_VERLET=1
;;   or
;;   $ unset SOLN_BY_VERLET
(if (System/getenv "SOLN_BY_VERLET")
  (do
    (println " Using Verlet method.")
    (def Verlet true))
  (do
    (println " Using RK4 method.")
    (def Verlet false)))


;;  main program
(let [L (+
         (constants :L0)
         (/ (* (constants :mbob)
               (constants :g))
            (constants :kspring)))
      ti 0.0                             ;; t_init
      tf 10.0                            ;; t_final
      h (/ (- tf ti) (constants :nsteps))]
  (loop [t    ti
         soln (vector
                (- L 0.01)               ;; r_init
                0.00
                (/ (constants :pi) 8.0)  ;; phi_init
                0.00)
         ; acc not used by RK4; manually unpack `soln` for Verlet
         acc  (forces [(soln 0) (soln 2)]
                      [(soln 1) (soln 3)]
                      [0 0]) ]
    (let [r      (soln 0)
          phi    (soln 2)]
      (spit "soln.txt"
            (str/join " " (list
              t
              (- r L)
              phi
              (* r (Math/sin phi))
              (* -1.0 (- (* r (Math/cos phi)) L))
              (energy soln)
              \newline))
            :append true)
      (when (< t tf)
        (if Verlet
          (let [[pos vel acc-next]    ;; destructure return
                (verlet-step h
                             [(soln 0) (soln 2)]
                             [(soln 1) (soln 3)]
                             acc
                             forces)]
            (recur (+ t h) [(pos 0) (vel 0) (pos 1) (vel 1)] acc-next))
          (recur (+ t h) (rk4step h soln Y) [0 0]))))))
                ;; time            pos vel   acc

