!!
!! parameters and types
!!
  module types
   use defs
   implicit none
   integer, parameter :: Nsteps = 8192           ! number of steps
   integer, parameter :: nr = 1, nphi = 4
   real(kind=rw), parameter :: g = 9.82D0, k = 1.000D1, L0 = 0.20D0
   real(kind=rw), parameter :: a = real(nr,kind=rw)*real(nr,kind=rw)/nphi/nphi
   real(kind=rw), parameter :: m = a/(1.0D0-a)*k*L0/g
   real(kind=rw), parameter :: pi = 3.1415926535897932384626433832795028841971D0
  end module types
!!
!! subprograms
!!
  module subs
   use defs
   use types
   implicit none

   contains

   ! the problem at hand; the vector f is r,rdot,phi,phidot
   pure function Y(f) result ( df )
    real(kind=rw), intent(in)  :: f(:)
    real(kind=rw), allocatable :: df(:)

    allocate( df( size(f) ) )     ! will `deallocate` when out of scope at `return`

    df(1) = f(2)
    df(2) = f(1)*f(4)*f(4) + g*cos(f(3)) - k/m*(f(1)-L0)
    df(3) = f(4)
    df(4) = (-1.0D0/f(1))*( 2.0D0*f(2)*f(4) + g*sin(f(3)) )

    return
   end function Y

  end module subs
!!
!! main
!!
  program main
    use defs
    use types
    use subs
    use rk4
    implicit none   
    integer, parameter :: p = 4          ! second deriv. times two dimensions
    real(kind=rw) :: soln(p), r(Nsteps), phi(Nsteps)
    real(kind=rw) :: ti, tf, h, t, L = L0 + m*g/k
    integer :: i 

    ! boundary conditions
    ti      = real(       0.0D0,    kind=rw )
    tf      = real(      10.0D0,    kind=rw )
    soln(1) = L * real(  +1.050,    kind=rw )     ! soln excludes r=0
    soln(2) = real(      +0.00D0,   kind=rw )     ! released from rest
    soln(3) = real(  +pi/32.00D0,   kind=rw ) 
    soln(4) = real(      +0.000D0,  kind=rw )     ! released from rest

    h = ( tf - ti ) / real(Nsteps,kind=rw)

    !  default `recl` record length is 40 characters per line; pgfortran will enforce!
    open(10,recl=180,file="soln.txt")
    do i = 1, Nsteps
     r(i)   = soln(1)
     phi(i) = soln(3)
     soln   = rk4step(h, soln, Y)
     t      = ti + h*( real(i,kind=rw) - real(1.0,kind=rw) )
     write(10,*) &
      &  t, r(i)-L, phi(i),  r(i)*sin(phi(i)), -(r(i)*cos(phi(i))-L)
    end do
    close(10)

    write(6,'(A,F8.5,A)') ' bob mass: ', m, ' kg;'
    write(6,'(A,F6.2,A,F6.2,A)')         &
     & ' natural pendulum period: ',     sqrt(g/L)/real(2.0,kind=rw)/pi, &
     & ' Hz; natural spring period: ',   sqrt(k/m)/real(2.0,kind=rw)/pi, ' Hz'

    !stop    ! pgfortran enforces IEEE_INEXACT upon `stop` ...
  end program main
