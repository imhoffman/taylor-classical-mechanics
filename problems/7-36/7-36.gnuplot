# http://ayapin-film.sakura.ne.jp/Gnuplot/Docs/ps_guide.pdf
##
##  first plot, r and phi versus time
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set key inside top left
#set key font ",18"
#set xtics font ",18"
#set ytics font ",18"
#set y2tics font ",18"
set output 'gnuplot.png'
set xzeroaxis
set xlabel 'time, {/:Italic t} (s)'
set ylabel 'radial position of bob, {/:Italic r-L} (m)' tc lt 1
set ytics nomirror tc lt 1
set yrange [+0.25:-0.25]
set y2label 'angular position of pendulum, {/Symbol f} (rad)' tc lt 2
set y2tics nomirror tc lt 2
set y2range [-pi/4.0:+pi/4.0]
#set format y2 "%.0P{/Symbol p}"
set y2tics ( "-{/Symbol p}/4" -pi/4.0, "-{/Symbol p}/8" -pi/8.0, "0" 0.0, "+{/Symbol p}/8" pi/8.0, "+{/Symbol p}/4" +pi/4.0)
plot 'soln.txt' using 1:2 with lines title 'numerical radius' axes x1y1 lt 1, 'soln.txt' using 1:3 with lines title 'numerical angle' axes x1y2 lt 2
reset
##
##  second plot, parametric spatial path
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set xtics font ",18"
set ytics font ",18"
set key inside top left
set output 'gnuplot2.png'
set xzeroaxis
set yzeroaxis
set size ratio -1
set xlabel '{/:Italic x} position of bob (m)'
set ylabel '{/:Italic y} position of bob (m)'
plot 'soln.txt' using 4:5 with lines title '' lc rgb "#FF00FF"
