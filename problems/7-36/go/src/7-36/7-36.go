package main

import (
	"bufio"
	"os"
	"log"
	"fmt"
	"math"
)

//  global constants
const (
	PI float64 = 3.1415926535897932384626433832795028841971
	P      int = 4		// two coords and their derivs
	Nsteps int = 8192
	G  float64 = 9.82	// gravity (m/s/s)
	M  float64 = 0.01358	// mass of bob (kg)
	K  float64 = 10.00	// stiffness of spring (N/m)
	L0 float64 = 0.20	// length of pendulum (m)
)

//
//  subprograms
//
//  vectorizing operations
func vec_scale ( a float64, v []float64 ) []float64 {
	w := make( []float64, len(v) )
	for i, y := range v { w[i] = a*y }
	return w
}

func vec_sum ( a []float64, b ... []float64 ) []float64 {
	c := make( []float64, len(a) )
	copy( c, a )
	for _, v := range b {
		if len(a) != len(v) { panic("dimension mismatch in vector sum") }
		for i, _ := range v { c[i] = c[i] + v[i] } }
	return c
}

//  the diff eqs of this problem
func Y ( f []float64 ) []float64 {
	df := make( []float64, len(f) )

	df[0] = f[1]
	df[1] = f[0]*f[3]*f[3] + G*math.Cos( f[2] ) - K/M*( f[0] - L0 )
	df[2] = f[3]
	df[3] = (-1.0/f[0])*( 2.0*f[1]*f[3] + G*math.Sin( f[2] ) )

	return df
}

//  fixed-step Runge--Kutta
func rkstep( h float64, vin []float64, YY func( []float64 ) []float64 ) []float64 {
 k1 := make( []float64, len(vin) )
 k2 := make( []float64, len(vin) )
 k3 := make( []float64, len(vin) )
 k4 := make( []float64, len(vin) )

 k1 = vec_scale( h, YY( vin ) )
 k2 = vec_scale( h, YY( vec_sum( vin, vec_scale( 1.0/2.0, k1 ) ) ) )
 k3 = vec_scale( h, YY( vec_sum( vin, vec_scale( 1.0/2.0, k2 ) ) ) )
 k4 = vec_scale( h, YY( vec_sum( vin, k3 ) ) )

 return vec_sum( vin,
          vec_scale( 1.0/6.0,
	    vec_sum( k1, k4, vec_scale( 2.0, k3 ), vec_scale( 2.0, k2 ) ) ) )
}


//  main program
func main () {
 var (
  r    [Nsteps]float64
  phi  [Nsteps]float64
  ti, tf, h, t float64
  L     float64 = L0 + M*G/K
  pY  func( []float64 ) []float64 = Y
 )

 soln := make( []float64, P )     // why doesn't go like this in var?

 //  boundary conditions
 ti =  0.0
 tf = 10.0
 soln[0] = L - 0.01		// r_0 with respect to L
 soln[1] = +0.00		// r dot
 soln[2] = +PI/32.00		// phi_0
 soln[3] = +0.00		// phi dot

 h = ( tf - ti ) / float64(Nsteps)

 //  open output file for writing
 file, err := os.Create("soln.txt")
 if err != nil { log.Fatal(err) }
 defer file.Close()
 s := bufio.NewWriter(file)

 //  solution loop
 for i:=0; i<Nsteps; i++ {
  r[i] = soln[0]
  phi[i] = soln[2]
  soln = rkstep( h, soln, pY )
  t = ti + h*float64(i)

  //  write line to file
  s.WriteString(
	 fmt.Sprintf("%v %v %v %v %v\n",
	 t, r[i]-L, phi[i], r[i]*math.Sin(phi[i]), -1.0*( r[i]*math.Cos(phi[i]) - L ) ) )
 } // solution loop

}

