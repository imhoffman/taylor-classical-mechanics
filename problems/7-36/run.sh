#!/bin/bash

prog="7-36"

if [ "$1" = "ifort" ]; then
  FC="ifort"
elif [ "$1" = "pgfortran" ]; then
  FC="pgfortran"
else
  FC="gfortran"
fi

rm -f soln.txt gnuplot*png
make clean
make FC=$FC

./$prog
gnuplot $prog.gnuplot
display gnuplot.png &
