!!
!! parameters and types
!!
  module types
   implicit none
   integer, parameter :: p = 4          ! second deriv. times two dimensions
   integer, parameter :: Nsteps = 8192  ! number of steps
   real(kind=16), parameter :: g=9.82Q0, pi = 3.1415926535897932384626433832795028841971Q0
   real(kind=16), parameter :: k = 1.498Q2, L0=0.20Q0, m=0.264Q0
  end module types
!!
!! subprograms
!!
  module subs
   use types
   implicit none

   contains

   ! the problem at hand; the vector f is r,rdot,phi,phidot
   function Y(f) result ( df )
    real(kind=16) :: f(p), df(p)

    df(1) = f(2)
    df(2) = f(1)*f(4)*f(4) + g*cos(f(3)) - k/m*(f(1)-L0)
    df(3) = f(4)
    df(4) = (-1.0Q0/f(1))*( 2.0Q0*f(2)*f(4) + g*sin(f(3)) )

    return
   end function Y

   ! generic Runge-Kutta step
   function step(h, v_n, func) result ( v_nplus1 )
    real(kind=16), intent(in)  :: h
    real(kind=16), intent(in)  :: v_n(p)
    procedure ( Y ), pointer :: func
    real(kind=16) :: v_nplus1(p)
    real(kind=16) :: k1(p), k2(p), k3(p), k4(p)

    k1 = h*func(v_n)
    k2 = h*func(v_n + k1/2.0Q0)
    k3 = h*func(v_n + k2/2.0Q0)
    k4 = h*func(v_n + k3)

    v_nplus1 = v_n + (k1 + 2.0Q0*k2 + 2.0Q0*k3 + k4)/6.0Q0
    return
   end function step

  end module subs
!!
!! main
!!
  program main
    use types
    use subs
    implicit none   
    real(kind=16) :: soln(p), r(Nsteps), phi(Nsteps)
    real(kind=16) :: ti, tf, h, t, L=L0+m*g/k
    procedure ( Y ), pointer :: pY => Y
    integer :: i 

    ! boundary conditions
    ti = 0.0Q0
    tf = 10.0Q0
    soln(1) = L -0.00Q0                 ! soln excludes r=0
    soln(2) = +0.00Q0                   ! released from rest
    soln(3) = +pi/04.00Q0 
    soln(4) = +0.000Q0                  ! released from rest

    h = (tf-ti)/dble(Nsteps)

    open(10,file="soln.txt")
    do i = 1,Nsteps
     r(i) = soln(1)
     phi(i) = soln(3)
     soln = step(h, soln, pY)
     t = ti+h*(dble(i)-1.0Q0)
     write(10,*) t, r(i)-L, phi(i),  r(i)*sin(phi(i)), -(r(i)*cos(phi(i))-L)
    end do
    close(10)
    write(6,'(A,F6.2,A,F6.2)') ' natural pendulum period: ', 2.0Q0*pi/sqrt(g/L) ,' natural spring period: ', 2.0Q0*pi/sqrt(k/m)

    stop
  end program main
