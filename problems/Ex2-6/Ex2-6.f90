!!
!! parameters and types
!!
  module types
   use defs
   implicit none
   integer, parameter       :: Nsteps = 1024  !!  number of steps
   real(kind=rw), parameter :: g  = 9.82_rw   !!  little g
   real(kind=rw), parameter :: pi = 3.14159265358979323846264338327950_rw
  end module types

!!
!! subprograms
!!
  module subs
   use defs
   use types
   implicit none

   contains

   !!  the problem at hand, with drag
   pure function vdot(f, rk4bool) result ( vec_out )
    real(kind=rw), intent(in)  :: f(:)
    logical, intent(in)        :: rk4bool
    real(kind=rw), allocatable :: df(:), vec_out(:)
    real(kind=rw)              :: m, c, gamma, D, cm
    integer                    :: ndim

    ndim = size(f)
    allocate( df(ndim) )
    !!  will `deallocate` when out of scope at `return`
    if ( rk4bool ) then
      allocate( vec_out( ndim ) )
    else
      allocate( vec_out(ndim/2) )
    endif

    m     = 0.15_rw      !!  mass
    gamma = 0.25_rw      !!  drag gamma
    D     = 0.07_rw      !!  drag D
    c     = gamma*D*D
    cm    = c/m

    df(1) = f(2)
    df(2) = -cm * sqrt( f(2)*f(2) + f(4)*f(4) ) *f(2)
    df(3) = f(4)
    df(4) = -g - cm * sqrt( f(2)*f(2) + f(4)*f(4) ) *f(4)

    if ( rk4bool ) then
      vec_out = df
    else
      vec_out = df(2::2)
    endif

    deallocate( df )
    return
   end function vdot

   !!  without drag
   pure function vdot1(f, rk4bool) result ( vec_out )
    real(kind=rw), intent(in)  :: f(:)
    logical, intent(in)        :: rk4bool
    real(kind=rw), allocatable :: df(:), vec_out(:)
    integer                    :: ndim

    ndim = size(f)
    allocate( df(ndim) )
    if ( rk4bool ) then
      allocate( vec_out( ndim ) )
    else
      allocate( vec_out(ndim/2) )
    endif

    df(1) = f(2)
    df(2) = 0.0_rw
    df(3) = f(4)
    df(4) = -g

    if ( rk4bool ) then
      vec_out = df
    else
      vec_out = df(2::2)
    endif

    deallocate( df )
    return
   end function vdot1
  end module subs

!!
!! main
!!
  program main
    use defs
    use types
    use subs
    use rk4
    use verlet
    implicit none   
    integer, parameter :: p = 4    !!  number of eqns in system
    real(kind=rw) :: soln(p), soln1(p)
    real(kind=rw) ::  acc_prev(p/2),  acc_next(p/2)
    real(kind=rw) :: acc1_prev(p/2), acc1_next(p/2)
    real(kind=rw) :: ti, tf, h, v0
    character     :: env_var
    logical       :: use_rk4
    integer       :: i 

    ! boundary conditions
    ti =   0.0_rw
    tf =  10.0_rw
    v0 = +30.0_rw

    soln(1) = 0.0_rw
    soln(2) = v0 * cos(50.0_rw*pi/180.0_rw) 
    soln(3) = 0.0_rw
    soln(4) = v0 * sin(50.0_rw*pi/180.0_rw) 
    soln1 = soln

    call getenv( "SOLN_BY_VERLET", env_var )
    if ( env_var .eq. " " ) then
      use_rk4 = .true.
      write(6,*) " Using Runge--Kutta solution method."
    else
      use_rk4 = .false.
      acc_next  =  vdot(  soln, .false. )
      acc1_next = vdot1( soln1, .false. )
      write(6,*) " Using Verlet solution method."
    endif


    !!
    !!  solution loop
    !!
    h = ( tf - ti ) / real(Nsteps,kind=rw)

    open(10,recl=180,file="drag.txt")
    open(11,recl=180,file="nodrag.txt")
    do i = 1, Nsteps
     if ( use_rk4 ) then
       call rk4step(h,  soln,  vdot)
       call rk4step(h, soln1, vdot1)
     else
       acc_prev = acc_next
       acc1_prev = acc1_next
       call verlet_step(h,  soln,  acc_prev,  acc_next,  vdot)
       call verlet_step(h, soln1, acc1_prev, acc1_next, vdot1)
     endif
     write(10,*)  soln(1::2)
     write(11,*) soln1(1::2)
    end do
    close(10)
    close(11)

    !stop
  end program main

