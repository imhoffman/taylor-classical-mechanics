# gnuplot script for program output
set terminal pngcairo enhanced size 1024,768 crop font "Helvetica, 24" linewidth 2
set output 'gnuplot.png'
set key inside bottom left
set xzeroaxis
set xlabel 'horizontal distance (m)'
set ylabel 'vertical distance (m)'
Every = 102                 # given 1024 points in code file
plot [0.0:+160.0] [-150.0:+40.0] 'drag.txt' with lines title 'with drag', 'drag.txt' using 1:2 every Every with points title '', 'nodrag.txt' with lines title 'without drag', 'nodrag.txt' using 1:2 every Every with points title ''
