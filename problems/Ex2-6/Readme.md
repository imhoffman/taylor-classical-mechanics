Based on [the exposition in Example 2.6](pages.pdf), the following reproduction of Figure 2.10 is obtained.
The fortran file [Ex2-6.f90](Ex2-6.f90) uses the general Runge&ndash;Kutta code in [the utilities directory](https://gitlab.com/imhoffman/taylor-classical-mechanics/tree/master/utilities) that is readily compiled with [the make script](run.sh).
The fortran file [standalone.f90](standalone.f90) is an older version with all of the code in a single file.

![gnuplot.png](gnuplot.png)
