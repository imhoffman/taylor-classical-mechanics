#!/bin/bash

prog="Ex2-6"

if [ "$1" = "ifort" ]; then
  FC="ifort"
elif [ "$1" = "pgfortran" ]; then
  FC="pgfortran"
else
  FC="gfortran"
fi

rm -f *drag.txt gnuplot.png
make clean
make FC=$FC

./$prog
gnuplot $prog.gnuplot
display gnuplot.png &

