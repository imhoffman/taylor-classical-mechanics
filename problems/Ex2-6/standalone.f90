!!
!! parameters and types
!!
  module types
   implicit none
   integer, parameter :: p = 4          ! number of coupled variables
   integer, parameter :: Nsteps = 1024  ! number of steps
  end module types
!!
!! subprograms
!!
  module subs
   use types
   implicit none

   contains

   ! the problem at hand
   function vdot(f) result ( df )
    real(kind=16) :: m=0.15Q0, g=9.82Q0, c, gm=0.25Q0, D=0.07Q0, cm
    real(kind=16) :: f(p), df(p)

    c = gm*D*D
    cm = c/m

    df(1) = f(2)
    df(2) = -cm * sqrt( f(2)*f(2) + f(4)*f(4) ) *f(2)
    df(3) = f(4)
    df(4) = -g - cm * sqrt( f(2)*f(2) + f(4)*f(4) ) *f(4)
    return
   end function vdot

   ! without drag
   function vdot1(f) result ( df )
    real(kind=16) :: g=9.82Q0
    real(kind=16) :: f(p), df(p)

    df(1) = f(2)
    df(2) = 0.0Q0
    df(3) = f(4)
    df(4) = -g
    return
   end function vdot1

   ! generic Runge-Kutta step
   function step(h, v_n, g) result ( v_nplus1 )
    real(kind=16), intent(in)  :: h
    real(kind=16), intent(in)  :: v_n(p)
    procedure ( vdot ), pointer :: g          ! use vdot rather than abstract interface
    real(kind=16) :: v_nplus1(p)
    real(kind=16) :: k1(p), k2(p), k3(p), k4(p)

    k1 = h*g(v_n)
    k2 = h*g(v_n + k1/2.0Q0)
    k3 = h*g(v_n + k2/2.0Q0)
    k4 = h*g(v_n + k3)

    v_nplus1 = v_n + (k1 + 2.0Q0*k2 + 2.0Q0*k3 + k4)/6.0Q0
    return
   end function step

   ! integrator, nothing fancy; simply RK steps
   function integ( f, n, h ) result ( area )
    integer, intent(in) :: n
    real(kind=16), intent(in) :: f(p, n), h
    real(kind=16) :: area(p)
    integer :: i

    ! rectangles
    area(:) = 0.0Q0
    do i = 1, n
     area = area + f(:,i)*h
    end do

    return
   end function integ

  end module subs
!!
!! main
!!
  program main
    use types
    use subs
    implicit none   
    real(kind=16) :: soln(p), v(p,Nsteps), x(p,Nsteps)
    real(kind=16) :: soln1(p), v1(p,Nsteps), x1(p,Nsteps)
    real(kind=16) :: ti, tf, h
    procedure ( vdot ), pointer :: pvdot => vdot
    procedure ( vdot1 ), pointer :: pvdot1 => vdot1
    integer :: i 

    ! boundary conditions
    ti = 0.0Q0
    tf = 10.0Q0
    soln(1) = 0.0Q0
    soln(2) = +30.0Q0*cos(50.0*3.14159/180.0) 
    soln(3) = 0.0Q0
    soln(4) = +30.0Q0*sin(50.0*3.14159/180.0) 
    soln1(1) = soln(1)
    soln1(2) = soln(2)
    soln1(3) = soln(3)
    soln1(4) = soln(4)

    h = (tf-ti)/dble(Nsteps)

    open(10,file="drag.txt")
    open(11,file="nodrag.txt")
    do i = 1,Nsteps
     v(1,i) = soln(2)
     v(2,i) = soln(4)
     x(1,i) = soln(1)
     x(2,i) = soln(3)
     soln = step(h, soln, pvdot)
     v1(1,i) = soln1(2)
     v1(2,i) = soln1(4)
     x1(1,i) = soln1(1)
     x1(2,i) = soln1(3)
     soln1 = step(h, soln1, pvdot1)
     write(10,*)  x(:,i)
     write(11,*) x1(:,i)
    end do
    close(10)
    close(11)

    stop
  end program main
