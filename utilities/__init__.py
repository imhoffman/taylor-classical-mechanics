import sys

if sys.version_info[0] < 3 or sys.version_info[1] < 6 :
  print("\n\n at least version 3.6 is required for fstrings\n\n")
  sys.exit(1)

import numpy as np

print("\n loading Python utilities\n\n")
