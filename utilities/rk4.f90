!! ode tools
!!  http://www.mathcs.emory.edu/~cheung/Courses/561/Syllabus/6-Fortran/modules.html

  module rk4
    use defs
    implicit none
    contains

   !!  fourth-order fixed-step Runge--Kutta
   !!  from Numerical Recipes
   !!  http://perso.fundp.ac.be/~amayer/Cours/ApprocheNumerique/chap16.pdf
   pure function rk4step(h, v_n, Y) result ( v_nplus1 )
    real(kind=rw), intent(in)  :: h
    real(kind=rw), intent(in)  :: v_n(:)
    real(kind=rw), allocatable :: v_nplus1(:)                ! cannot have `intent(out)`
    real(kind=rw), allocatable :: k1(:), k2(:), k3(:), k4(:)
    integer :: Neq
    interface
      pure function Y ( f )
        use defs
        real(kind=rw), allocatable  :: Y(:)
        real(kind=rw), intent(in)  :: f(:)
      end function Y
    end interface

    Neq = size( v_n )
    allocate( v_nplus1(Neq), k1(Neq), k2(Neq), k3(Neq), k4(Neq) )

    k1 = h * Y( v_n )
    k2 = h * Y( v_n + k1/real(2.0,kind=rw) )
    k3 = h * Y( v_n + k2/real(2.0,kind=rw) )
    k4 = h * Y( v_n + k3 )

    v_nplus1 = v_n + &
   &  ( k1 + real(2.0,kind=rw)*k2 + real(2.0,kind=rw)*k3 + k4 ) &
   &   /real(6.0,kind=rw)

    return
   end function rk4step

  end module rk4
