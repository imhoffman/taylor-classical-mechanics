# Runge--Kutta stuff

def rk4step ( h, v_n, Y ):
    k1 = h * Y( v_n )
    k2 = h * Y( v_n + k1/2.0 )
    k3 = h * Y( v_n + k2/2.0 )
    k4 = h * Y( v_n + k3 )

    return v_n + ( k1 + 2.0*k2 + 2.0*k3 + k4 )/6.0

