#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"taylor.h"
#include<omp.h>

//
//  vector arithmetic utilities
//
//   scalar multiplication
//    overwrites input vector
void
vec_scale ( const double a, double *v, const int length_v )
{
  int i;
  for ( i=0; i<length_v; i++ ) {
    *(v+i) = *(v+i) * a;
  }
  return;
}

//   two-vector sum
//    assumes input vecs of same length
//    overwrites first argument
void
vec_sum ( double *a, const double *b, const int length )
{
  int i;
  #pragma omp parallel for \
              shared ( a, b, length ) \
              private ( i ) \
              default ( none ) \
              schedule ( static ) \
              num_threads ( length < MULTITHREAD_LENGTH_LIMIT ? \
		            1 : omp_get_num_procs() )
  for ( i=0; i<length; i++ ) {
    *(a+i) = *(a+i) + *(b+i);
  }
  return;
}


//
//  Runge-Kutta: fixed-step, fourth-order
//   overwrites x_in, v_in
void
rk4step( const double h,
         double       *x_in,
         double       *v_in,
	 const int    length_x,
	 void   (*Y)( double    *xv,
	              double    *a,
		      const int length_xv ) )
{

 int length_dim = 2*length_x;
 size_t size_dim = sizeof(double)*length_dim;
 double *tmp, *xv0, *k1, *k2, *k3, *k4;
 double *dummy;

 tmp   =(double *) malloc(  size_dim  );
 xv0   =(double *) malloc(  size_dim  );
 k1    =(double *) malloc(  size_dim  );
 k2    =(double *) malloc(  size_dim  );
 k3    =(double *) malloc(  size_dim  );
 k4    =(double *) malloc(  size_dim  );
 //  dummy return is not used: for consistency with Verlet Y acc
 dummy =(double *) malloc( size_dim/2 );

 // populate tmp, xv0 with the inputs
 for ( int i=0; i<length_x; i++ ) {
   tmp[2*i+0] = x_in[i];
   tmp[2*i+1] = v_in[i];
 }
 memcpy( xv0, tmp, size_dim );

 Y( tmp, dummy, length_dim );        // tmp returns from Y
 memcpy( k1, tmp, size_dim );        // the returned tmp is copied to k1
 vec_scale( h, k1, length_dim );     // k1 is scaled and kept

 memcpy( tmp, k1, size_dim );
 vec_scale( 0.5, tmp, length_dim );  // k1/2
 vec_sum( tmp, xv0, length_dim );    // v + k1/2
 Y( tmp, dummy, length_dim );
 memcpy( k2, tmp, size_dim );
 vec_scale( h, k2, length_dim );
 
 memcpy( tmp, k2, size_dim );
 vec_scale( 0.5, tmp, length_dim );
 vec_sum( tmp, xv0, length_dim );
 Y( tmp, dummy, length_dim );
 memcpy( k3, tmp, size_dim );
 vec_scale( h, k3, length_dim );
 
 memcpy( tmp, k3, size_dim );
 vec_sum( tmp, xv0, length_dim );
 Y( tmp, dummy, length_dim );
 memcpy( k4, tmp, size_dim );
 vec_scale( h, k4, length_dim );

 vec_scale( 2.0, k2, length_dim );
 vec_scale( 2.0, k3, length_dim );
 vec_sum( k2, k3, length_dim );
 vec_sum( k2, k4, length_dim );
 vec_sum( k2, k1, length_dim );
 vec_scale( 1./6., k2, length_dim );
 vec_sum( xv0, k2, length_dim );     // update xv

 // unpack xv into x_in, v_in with the outputs
 for ( int i=0; i<length_x; i++ ) {
   x_in[i] = xv0[2*i+0];
   v_in[i] = xv0[2*i+1];
 }

 free( tmp ); free( xv0 );
 free( k1 ); free( k2 ); free( k3 ); free( k4 );
 free( dummy );
 
 return;
}


//
//  velocity Verlet
//
void
verlet_step( const double h,
                   double *x,
	           double *v,
	     const double *ainit,
	           double *afinal,
	     const int    ndim,
	     void   (*Y)( double     *xv,
	                  double    *acc,
		          const int length_xv ) )
{
  size_t vec_size = ndim*sizeof( double );
  double *vmid =(double *) malloc(  vec_size  );
  double *temp =(double *) malloc(  vec_size  );
  double *xv   =(double *) malloc( 2*vec_size );

  memcpy( vmid, ainit, vec_size );
  vec_scale( 0.5*h, vmid, ndim );
  vec_sum( vmid, v, ndim );         // vmid = v + 0.5*h*ainit

  memcpy( temp, vmid, vec_size );
  vec_scale( h, temp, ndim );
  vec_sum( x, temp, ndim );         // x = x + h*vmid

  memcpy( afinal, ainit, vec_size );
  //  load up an `xv` vec for consistency with RK4's call to Y
  for ( int i=0; i<ndim; i++ ) { xv[2*i+0] = x[i]; xv[2*i+1] = v[i]; }
  Y( xv, afinal, 2*ndim );
  memcpy( temp, afinal, vec_size ); // afinal is kept
  vec_scale( 0.5*h, temp, ndim );
  vec_sum( vmid, temp, ndim );      //     vmid + 0.5*h*afinal
  memcpy( v, vmid, vec_size );      // v = vmid + 0.5*h*afinal

  free( vmid ); free( temp ); free( xv );
  return;
}

