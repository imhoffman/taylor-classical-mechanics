
;; linear-algebra utilities with no Java interop

;;  how best to get the list of pairs into a vector?
;;  https://stackoverflow.com/questions/12044181/into-or-vec-converting-sequence-back-to-vector-in-clojure
;;  but ... https://guide.clojure.style/#to-vector

(defn vec-scale
  "multiply every element of vector V by scalar factor a"
  [a V]
  (assert (vector? V) "vec-scale: argument V should be a vector")
  (assert (number? a) "vec-scale: argument a should be a scalar")
  ;(into [] (for [element V] (* element a))))
  (loop [V-stack V
         accum   (transient [])]
    (if (empty? V-stack)
      (persistent! accum)
      (recur (rest V-stack) (conj! accum (* a (first V-stack)))))))


(defn vec-sum
  "add two or more vectors element-wise, returning a vector"
  [A B & Cs]
  (assert (= (count A) (count B)) "vec-sum: vectors not the same length")
  (if (not Cs)
    ;(vec (for [pair (zipmap A B)] (+ (key pair) (val pair))))
    ;(vec (for [pair (map vector A B)] (apply + pair)))
    (loop [zip-stack (map vector A B)
           accum     (transient [])]
      (if (empty? zip-stack)
        (persistent! accum)
        (recur (rest zip-stack) (conj! accum (apply + (first zip-stack))))))
    ;(into [] (for [pair (map vector A B)] (apply + pair)))
    (case (count Cs)
      1 (vec-sum A (vec-sum B (first Cs)))
      2 (vec-sum A (vec-sum B (first Cs) (last Cs)))
        (vec-sum A B (apply vec-sum Cs)))))


(defn vec-dot
  "the dot product of two vectors, returning a scalar"
  [A B]
  (assert (= (count A) (count B)) "vec-dot: vectors not the same length")
  (apply +
         (for [pair (map vector A B)] (apply * pair))))


(defn matrix-matrix-product
  "multiplication of two 2D matrices, returning a vector of vectors:
   Inputs must be square (not jagged) vectors of vectors."
  [A B]
  (assert (= (count (A 0)) (count B)) "matrix-product: dimension mismatch")
    (loop [i 0           ;; the row of A being addressed
           j 0           ;; the col of B being addressed
           row-accum []  ;; empty vectors, not lists
           C-accum []]
      (let [row-A (A i)
            col-B (into [] (for [k (range (count B))] ((B k) j)))
            current-row (conj row-accum (vec-dot row-A col-B))]
        (if (< (+ j 1) (count (B 0)))
            (recur i (inc j) current-row C-accum)
            (let [C (conj C-accum current-row)]
              (if (< (+ i 1) (count A))
                (recur (inc i) 0 [] C)
                C))))))


(defn matrix-vector-product
  "multiplication of a 2D matrix with a 1D vector, returning a vector"
  [A V]
  (assert (= (count (A 0)) (count V)) "matrix-product: dimension mismatch")
    (loop [i 0               ;; the row of A being addressed
           C-accum (transient [])]
      (let [row-A (A i)
            C     (conj! C-accum (vec-dot row-A V))]
        (if (< (+ 1 i) (count A))
          (recur (inc i) C)
          (persistent! C)))))


;;  ODE tools

(defn rk4step
  "fixed-step, fourth-order Runge--Kutta
   returns updated vector of coords and derivs
   h:     time step
   vecin: vector of coord1, coord1-dot, coord2, coord2-dot, etc.
   Y:     function of vecin that contains the diff eqs"
  [h vecin Y]
  (let [k1 (vec-scale h (Y vecin))
        k2 (vec-scale h (Y (vec-sum vecin (vec-scale 0.5 k1))))
        k3 (vec-scale h (Y (vec-sum vecin (vec-scale 0.5 k2))))
        k4 (vec-scale h (Y (vec-sum vecin k3)))]
    (vec-sum
      vecin
      (vec-scale
        (/ 1.0 6.0)
        (vec-sum
          k1
          (vec-scale 2.0 k2)
          (vec-scale 2.0 k3)
          k4))))
  )


(defn verlet-step
  "velocity Verlet
   returns updated vector of coords, derivs, and latest computed acc
   h: time step
   pos:      vector of coords
   vel:      vector of coord derivs
   acc-prev: computed acceleration from previous step
   acc-next: acceleration to be re-used next step
   forces:   function of pos, vel, and acc that contains the diff eqs"
  [h pos vel acc-prev forces]
  (let [vmid     (vec-sum vel (vec-scale (* 0.5 h) acc-prev))
        x        (vec-sum pos (vec-scale h vmid))
        acc-next (forces x vel acc-prev)
        v        (vec-sum vmid (vec-scale (* 0.5 h) acc-next))]
    (vector x v acc-next)))

