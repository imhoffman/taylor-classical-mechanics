
 !!  hardware definitions ... bit widths
 module defs
  use iso_fortran_env
  implicit none
  integer, parameter :: rw = real64
  integer, parameter :: iw = int32
 end module defs

 !!
 !!  in both RK4 and Verlet below, the same
 !!   function Y that contains the diff eq system
 !!   is used for both RK4 and Verlet as per
 !!   an input LOGICAL
 !!

 !!  fourth-order fixed-step Runge--Kutta
 !!   from Numerical Recipes
 !!   http://perso.fundp.ac.be/~amayer/Cours/ApprocheNumerique/chap16.pdf
 module rk4
   use defs
   implicit none
   contains

   pure subroutine rk4step(h, xv, Y)
    real(kind=rw), intent(in)    :: h
    real(kind=rw), intent(inout) :: xv(:)
    real(kind=rw), allocatable   :: k1(:), k2(:), k3(:), k4(:)
    integer                      :: Neq
    interface
      pure function Y ( posvel, odebool )
        use defs
        real(kind=rw), allocatable :: Y(:)
        real(kind=rw), intent(in)  :: posvel(:)
        logical, intent(in)        :: odebool
      end function Y
    end interface

    Neq = size( xv )
    allocate( k1(Neq), k2(Neq), k3(Neq), k4(Neq) )

    k1 = h * Y( xv,             .true. )  !!  true for RK4 return
    k2 = h * Y( xv + k1/2.0_rw, .true. )
    k3 = h * Y( xv + k2/2.0_rw, .true. )
    k4 = h * Y( xv + k3,        .true. )

    xv = xv + &
   &  ( k1 + 2.0_rw*k2 + 2.0_rw*k3 + k4 ) / 6.0_rw

    deallocate( k1, k2, k3, k4 )
    return
   end subroutine rk4step
 end module rk4


 !! velocity Verlet
 !!  https://www.feynmanlectures.caltech.edu/I_09.html
 module verlet
   use defs
   implicit none
   contains

   pure subroutine verlet_step ( DeltaT, xv, ainit, afinal, Y )
    real(kind=rw), intent(in)    :: ainit(:), DeltaT
    real(kind=rw), intent(inout) :: xv(:), afinal(:)
    real(kind=rw), allocatable   :: xvmid(:)
    interface
      pure function Y ( posvel, odebool )
        use defs
        real(kind=rw), allocatable :: Y(:)
        real(kind=rw), intent(in)  :: posvel(:)
        logical, intent(in)        :: odebool
      end function Y
    end interface

    !!  xv is [ x_1, v_1, x_2, v_2, ... ]
    !!  using monolithic xv for compat with RK4
    allocate( xvmid( size(xv) ) )

    !!  determine v_mid and use it to find x_mid
    xvmid(2::2) = xv(2::2) +  0.5_rw * DeltaT * ainit
    xvmid(1::2) = xv(1::2) +           DeltaT * xvmid(2::2)

    !!  update acc with x_mid and v_mid
    afinal = Y( xvmid, .false. )  !! .false. for Verlet func

    !!  update v_out and load up x_out
    xv(2::2) = xvmid(2::2) + 0.5_rw * DeltaT * afinal
    xv(1::2) = xvmid(1::2)

    deallocate( xvmid )

    !!  xv and afinal get returned
    return
   end subroutine verlet_step
 end module verlet

