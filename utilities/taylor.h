#define MULTITHREAD_LENGTH_LIMIT 256

//
//  vector arithmetic utilities
//
//   scalar multiplication
//    overwrites input vector
void
vec_scale ( const double a, double *v, const int length_v );

//   two-vector sum
//    assumes input vecs of same length
//    overwrites first argument
void
vec_sum ( double *a, const double *b, const int length );


//
//  Runge-Kutta: fixed-step, fourth-order
//   overwrites x_in, v_in
void
rk4step( const double h,
         double       *x_in,
         double       *v_in,
	 const int    length_x,
	 void   (*Y)( double    *xv,
	              double    *a,
		      const int length_xv ) );

//
//  velocity Verlet
//
void
verlet_step( const double h,
                   double *x,
	           double *v,
	     const double *ainit,
	           double *afinal,
	     const int    ndim,
	     void   (*Y)( double     *xv,
	                  double    *acc,
		          const int length_xv ) );

