!! ode tools
!!  https://www.feynmanlectures.caltech.edu/I_09.html

  module verlet
    use defs
    implicit none
    contains

   !!  velocity Verlet
   pure subroutine verlet_step ( DeltaT, x, v, ainit, afinal, F )
    real(kind=rw), intent(in)    :: ainit(:), DeltaT
    real(kind=rw), intent(inout) :: x(:), v(:), afinal(:)
    real(kind=rw), allocatable   :: vmid(:)
    integer                      :: ndim
    interface
      pure subroutine F ( pos, vel, acc )
        use defs
        real(kind=rw), intent(in)    :: pos(:), vel(:)
        real(kind=rw), intent(inout) :: acc(:)
      end subroutine F
    end interface

    ndim = size( v )
    allocate( vmid( ndim ) )

    vmid = v    + real(0.5,kind=rw) * DeltaT * ainit
    x    = x    +                     DeltaT * vmid
    afinal = ainit
    call F( x, v, afinal )       ! update a
    v    = vmid + real(0.5,kind=rw) * DeltaT * afinal

    deallocate( vmid )

    return
   end subroutine verlet_step

  end module verlet
